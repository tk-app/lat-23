$(document).ready(function(){
	
	window.onload = iStripInit;
	// if images would have width attribute set... this could be read out and we wouldn't have to wait	
	// for the elements to load
	// this could then differentiate between type (withlink, without, div or img) 
	// width from div should be read from style (.width() this should exist before page load has completed
	//$('#ipreview').hide();	
	
	$('div#pubimg').each(function() {
		new pubImg(this);
	});
	
	
	//implement reset default values for forms just search form at the moment
	/*$("input[@type='text']").each(function() {
		$(this).attr('default',$(this).val());
	});
	$("input[@type='text']").focus(function() {
		if ($(this).attr('default') == $(this).val()){$(this).val('');}
	});
	$("input[@type='text']").blur(function() {
		if ($(this).val() == '') {$(this).val($(this).attr('default'));}
	});*/
});


function pubImg(container) {
	this.me = $(container);
	this.strip = $(this.me.find('div'));
	this.slngth = this.me.width();
	this.widths = [];
	this.strip.css('margin-left','1px');
	this.lngth = 0;
	this.cpos = 0;
	this.btn = new Array();
	this.act = [true,true];
	this.init();
	return this;

}
pubImg.prototype.init = function() {
	var sobj =  this;
	this.me.find('img').each(function(){
		sobj.lngth++;

		$(this).removeClass('simg');
		var templ = $(this).attr('width')
		sobj.widths.push((parseInt(templ,10)+2));
		//alert(sobj.widths[sobj.widths.length-1]);
	});
	this.strip.css('width',(sobj.lngth+1)*this.slngth);
	
	if (this.lngth > 1) {
		this.me.before('<p class="slideNav"><a class="preva" title="Previous image"><span>Previous image</span></a><a class="nexta" title="Next image"><span>Next image</span></a><span class="text">'+this.lngth+' Images</span></p>');
		this.btn[0] = $($('p.slideNav').find('a.preva'));
		this.btn[1] = $($('p.slideNav').find('a.nexta'));
		this.btn[0].click(function(me){sobj.move(-1);});
		this.btn[1].click(function(me){sobj.move(1);});
		this.ckB();
	}
}

pubImg.prototype.ckB = function() {
	if (this.cpos == 0) {
		this.btn[0].addClass('notactive');
		this.act[0] = false;
		this.act[1] = true;
		this.btn[1].removeClass('notactive');
	} else if (this.cpos == (this.lngth-1)) {
		this.btn[1].addClass('notactive');
		this.act[1] = false;
		this.act[0] = true;
		this.btn[0].removeClass('notactive');
	} else {
		this.btn[0].removeClass('notactive');
		this.btn[1].removeClass('notactive');
		this.act[0] = true;
		this.act[1] = true;
	}
}
pubImg.prototype.move = function(dir) {

	if (dir == 1) {
		if (this.cpos < (this.lngth-1) && this.act[1]) {
			this.cpos++;
			var npos = 0;
			for (var i=0;i<this.cpos;i++) {
				npos -= this.widths[i];
			}
			
			//var npos = -((this.cpos)*this.slngth);
			this.strip.animate({marginLeft:npos},'slow');
		}
	} else {
		if (this.cpos > 0 && this.act[0]) {
			this.cpos--;
			var npos = 0;
			for (var i=0;i<this.cpos;i++) {
				npos -= this.widths[i];
			}
			//var npos = -((this.cpos)*this.slngth);
			this.strip.animate({marginLeft:npos},'slow');
		}
	}	
	this.ckB();
	
}



function iStripInit() {
	$('div.media').each(function(){
		new iStrip(this);
	});
}

function iStrip(container) {
	this.me = $(container);
	this.strip = $(this.me.find('div.strip'));
	this.slngth = this.strip.css('width');
	this.slngth = this.slngth.substring(0,this.slngth.indexOf('p'));
	this.slnght = parseInt(this.slngth);
	//$('div.media').after(this.slngth+'<br />');
	this.strip.css('margin-left','1px');
	this.imgs = new Array();
	this.gap = null;
	this.lngth = 0;
	this.cpos = 0;
	this.max = null;
	this.maxp = null;
	this.btn = new Array();
	this.act = [true,true];
	this.init();
	return this;
}
iStrip.prototype.init = function() {

		//$('div#ipreview').hide();
		//alert($('div#ipreview').length);
		
		var sobj = this;
		this.strip.find('a').each(function(){
			sobj.addImg($($(this).children()));
			
			
			//alert(wURL);
			
			
		});

		this.me.find('.items').each(function(){
			var obj = $(this);
			obj.find('div.strip').css('width',sobj.getW());
		});
		
		this.max = -((this.lngth-6) - this.slngth);
		this.maxp = 0;
		var l = this.imgs.length;
		for(this.maxp=0;this.maxp<l;this.maxp++) {
			if (this.max >= this.imgs[this.maxp][2]) {
				break;
			}
		}
		
		
		
		if (this.lngth > this.slngth) {
			this.me.find('h2').after('<p><a class="preva" title="Previous image"><span>Previous image</span></a><a class="nexta" title="Next image"><span>Next image</span></a></p>');
			this.btn[0] = $(this.me.find('a.preva'));
			this.btn[1] = $(this.me.find('a.nexta'));
			this.btn[0].click(function(me){sobj.move(-1);});
			this.btn[1].click(function(me){sobj.move(1);});
			this.ckB();
		} 
		

		
}
iStrip.prototype.addImg = function(elm) {
		var w = parseInt(elm.width());
	//	$('div.media').after(w+'<br />');
		if (this.gap == null) {
			this.gap = parseInt(elm.parent('a').css('margin-right'));
		}
		this.imgs.push([elm,-w,-this.lngth,elm.parent('a').attr('href')]);
		this.lngth +=  w + this.gap;
}
	
iStrip.prototype.getW = function() {
		return this.lngth+this.gap;
	}
iStrip.prototype.move = function(dir) {

	if (dir == 1) {
		if (this.cpos < this.maxp && this.act[1]) {
			this.cpos++;
			var npos = this.imgs[this.cpos][2];
			if (-(npos) > (this.lngth-this.gap-this.slngth)) {
				npos = -(this.lngth-this.gap-this.slngth);	
			}
			this.strip.animate({marginLeft:npos},'slow');
		}
	} else {
		if (this.cpos > 0 && this.act[0]) {
			this.cpos--;
			var npos = this.imgs[this.cpos][2];
			this.strip.animate({marginLeft:npos},'slow');
		}
	}	
	this.ckB();
	
}
iStrip.prototype.ckB = function() {
	if (this.cpos == 0) {
		this.btn[0].addClass('notactive');
		this.act[0] = false;
		this.act[1] = true;
		this.btn[1].removeClass('notactive');
	} else if (this.cpos == this.maxp) {
		this.btn[1].addClass('notactive');
		this.act[1] = false;
		this.act[0] = true;
		this.btn[0].removeClass('notactive');
	} else {
		this.btn[0].removeClass('notactive');
		this.btn[1].removeClass('notactive');
		this.act[0] = true;
		this.act[1] = true;
	}
}
