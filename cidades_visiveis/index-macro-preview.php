<?
	$macro_id = $_GET['macro_id'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="description" content="description" />
<meta name="keywords" content="keywords" />
<title>CIDADES VISÍVEIS</title>
<script type="text/javascript" src="cidades-visiveis.js"></script>
<script type="text/javascript" src="swfobject.js"></script>
<style type="text/css">
	
	body {
		background-color: #000000;
		margin-left: 0px;
		margin-top: 0px;
		margin-right: 0px;
		margin-bottom: 0px;
	}
	
	#flashcontent {
		margin: 0px auto;
		width: 1000px;
		height: 650px;
		display: block;
	}

</style>
</head>
<body>

	<div id="flashcontent">
		<strong>Atualizar Flash</strong>
		<a href="http://www.adobe.com/go/gntray_dl_getflashplayer">Clique aqui para atualizar sua versão do flash</a>
	</div>
	
	<script type="text/javascript">	
		var so = new SWFObject("cidades-visiveis-macro-preview.swf?macro_id=<? echo $macro_id; ?>", "cidades-visiveis", "1000", "650", "8", "#000000");
		so.addParam('allowfullscreen','true'); 
		so.write("flashcontent");
	</script>
	
	<script type="text/javascript">

	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-17178766-1']);
	  _gaq.push(['_trackPageview']);

	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();

	</script>
	
</body>
</html>