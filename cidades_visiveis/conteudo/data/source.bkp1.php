<?

include '../../connect.php';

$tag = $_GET['tag'];

$resposta  = '<?xml version="1.0" encoding="utf-8"?>';
$resposta .= "\n";
$resposta .= "\n";

$resposta .= "<cidades_visiveis> \n";

$resposta .= "	<config> \n";
$resposta .= "		<nome><![CDATA[Cidades Visíveis]]></nome> \n";
$resposta .= "	</config> \n";

$resposta .= "	<cameras> \n";

$resposta .= "		<norte> \n";
$sql = "SELECT a.id, a.titulo, a.url, a.tipo, a.latitude, a.longitude
	    FROM cameras a, camera_tag b
	    WHERE a.hemisferio = 'norte'
	    AND a.publicado = 1
	    AND a.id = b.id_camera
	    AND b.id_tag = $tag
		ORDER BY rand() 
		LIMIT 4";
$res = mysql_query($sql, $conn);
$qts = mysql_num_rows($res);
for($x=0; $x<$qts; $x++){
	$id        = mysql_result($res, $x, 0);
	$titulo    = mysql_result($res, $x, 1);
	$url       = mysql_result($res, $x, 2);
	$tipo      = mysql_result($res, $x, 3);
	$latitude  = mysql_result($res, $x, 4);
	$longitude = mysql_result($res, $x, 5);
	$resposta .= "			<camera active='0' url='$url' latide='$latitude' longitude='$longitude' tipo='$tipo'> \n";
	$resposta .= "				<nome><![CDATA[$titulo]]></nome> \n";
	$resposta .= "			</camera> \n";
}
$resposta .= "		</norte> \n";

$resposta .= "		<sul> \n";
$sql = "SELECT a.id, a.titulo, a.url, a.tipo, a.latitude, a.longitude
	    FROM cameras a, camera_tag b
	    WHERE a.hemisferio = 'sul'
	    AND a.publicado = 1
	    AND a.id = b.id_camera
	    AND b.id_tag = $tag
		ORDER BY rand() 
		LIMIT 4";
$res = mysql_query($sql, $conn);
$qts = mysql_num_rows($res);
for($x=0; $x<$qts; $x++){
	$id        = mysql_result($res, $x, 0);
	$titulo    = mysql_result($res, $x, 1);
	$url       = mysql_result($res, $x, 2);
	$tipo      = mysql_result($res, $x, 3);
	$latitude  = mysql_result($res, $x, 4);
	$longitude = mysql_result($res, $x, 5);
	$resposta .= "			<camera active='0' url='$url' latide='$latitude' longitude='$longitude' tipo='$tipo'> \n";
	$resposta .= "				<nome><![CDATA[$titulo]]></nome> \n";
	$resposta .= "			</camera> \n";
}
$resposta .= "		</sul> \n";

$resposta .= "	</cameras> \n";

$resposta .= "</cidades_visiveis> \n";


print $resposta;

?>
