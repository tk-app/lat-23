<?

include '../../connect.php';

$resposta  = '<?xml version="1.0" encoding="utf-8"?>';
$resposta .= "\n";
$resposta .= "\n";

$resposta .= "<cidades_visiveis> \n";

$previous = 0;

for($x=0; $x<18; $x++){
	
	$grid      = rand(1,3);	
	$duracao   = rand(1,3) * 20;
	$kind      = rand(1,2);
	
	if($x == 0){
		$duracao = 20;
		$kind = 1;
	}
	
	if($grid == $previous){
		if($grid == 1){
			$grid = 2;
		}else if($grid == 2){
			$grid = 3;
		}else if($grid == 3){
			$grid = 1;
		}
	}
	
	$previous = $grid;
	
	$resposta .= "	<scene id='$x' grid='$grid' previous='$previous' duracao='$duracao' corrido='0'> \n";	
	
	if($grid == 1){
		$limit_1 = 3;
		$limit_2 = 3;
	}else if($grid == 2){
		$limit_1 = 3;
		$limit_2 = 3;
	}else if($grid == 3){
		$limit_1 = 3;
		$limit_2 = 3;
	}
	
	$sql = "SELECT a.id, a.tag
			FROM tags a, camera_tag b, cameras c
			WHERE b.id_tag = a.id
			AND b.id_camera = c.id
			AND c.publicado =1
			GROUP BY a.id
			ORDER BY RAND() 
			LIMIT 2"; 
	$res = mysql_query($sql, $conn);
	if($res){
		$tag_id_1 = mysql_result($res, 0, 0);
		$tag_nm_1 = mysql_result($res, 0, 1);
		$tag_id_2 = mysql_result($res, 1, 0);
		$tag_nm_2 = mysql_result($res, 1, 1);
	}
	
	$resposta .= "		<tag item='1' id='$tag_id_1'> \n";
	$resposta .= "			<nome><![CDATA[$tag_nm_1]]></nome> \n";
	$resposta .= "			<cameras> \n";
	
	if($kind == 1){
		$sql_c = "SELECT a.id, a.titulo, a.url, a.tipo, a.latitude, a.longitude
			      FROM cameras a, camera_tag b
			      WHERE a.publicado = 1
			      AND a.id = b.id_camera
			      AND b.id_tag = $tag_id_1
				  AND a.titulo like '%PRE%'
				  ORDER BY rand() 
				  LIMIT $limit_1";
	}else{
		$sql_c = "SELECT a.id, a.titulo, a.url, a.tipo, a.latitude, a.longitude
			      FROM cameras a, camera_tag b
			      WHERE a.publicado = 1
			      AND a.id = b.id_camera
			      AND b.id_tag = $tag_id_1
				  AND a.titulo like '%VIVO%'
				  ORDER BY rand() 
				  LIMIT $limit_1";
	}
	$sql_c = "SELECT a.id, a.titulo, a.url, a.tipo, a.latitude, a.longitude
		      FROM cameras a, camera_tag b
		      WHERE a.publicado = 1
		      AND a.id = b.id_camera
		      AND b.id_tag = $tag_id_1
			  ORDER BY rand() 
			  LIMIT $limit_1";
			
	$res_c = mysql_query($sql_c, $conn);
	$qts_c = mysql_num_rows($res_c);
	for($c=0; $c<$qts_c; $c++){
		$id        = mysql_result($res_c, $c, 0);
		$titulo    = mysql_result($res_c, $c, 1);
		$url       = mysql_result($res_c, $c, 2);
		$tipo      = mysql_result($res_c, $c, 3);
		$resposta .= "				<camera item='$x-1-$c' id='$id' active='0' url='$url' tipo='$tipo'> \n";
		$resposta .= "					<nome><![CDATA[$titulo]]></nome> \n";
		$resposta .= "					<imagens> \n";
		$sql_2     = "SELECT arquivo FROM camera_frames WHERE id_camera = $id ORDER BY id ASC";
		$res_2     = mysql_query($sql_2, $conn);
		if($res_2){
			$quantos_2 = mysql_num_rows($res_2);
			for($y=0; $y<$quantos_2; $y++){
				$arquivo = mysql_result($res_2, $y, 0);
				$caminho = "../frames/$arquivo";
				if(is_file($caminho)){
					$resposta .= "						<imagem arquivo='$arquivo' /> \n";
				}
			}
		}
		$resposta .= "					</imagens> \n";
		$resposta .= "				</camera> \n";
	}
	$resposta .= "			</cameras> \n";
	$resposta .= "		</tag> \n";
	
	$resposta .= "		<tag item='2' id='$tag_id_2'> \n";
	$resposta .= "			<nome><![CDATA[$tag_nm_2]]></nome> \n";
	$resposta .= "			<cameras> \n";
	
	if($kind == 1){
		$sql_c = "SELECT a.id, a.titulo, a.url, a.tipo, a.latitude, a.longitude
			      FROM cameras a, camera_tag b
			      WHERE a.publicado = 1
			      AND a.id = b.id_camera
			      AND b.id_tag = $tag_id_2
				  AND a.titulo like '%PRE%'
				  ORDER BY rand() 
				  LIMIT $limit_2";
	}else{
		$sql_c = "SELECT a.id, a.titulo, a.url, a.tipo, a.latitude, a.longitude
			      FROM cameras a, camera_tag b
			      WHERE a.publicado = 1
			      AND a.id = b.id_camera
			      AND b.id_tag = $tag_id_2
				  AND a.titulo like '%VIVO%'
				  ORDER BY rand() 
				  LIMIT $limit_2";
	}
	
	$sql_c = "SELECT a.id, a.titulo, a.url, a.tipo, a.latitude, a.longitude
		      FROM cameras a, camera_tag b
		      WHERE a.publicado = 1
		      AND a.id = b.id_camera
		      AND b.id_tag = $tag_id_2
			  ORDER BY rand() 
			  LIMIT $limit_2";
			
	$res_c = mysql_query($sql_c, $conn);
	$qts_c = mysql_num_rows($res_c);
	for($c=0; $c<$qts_c; $c++){
		$id        = mysql_result($res_c, $c, 0);
		$titulo    = mysql_result($res_c, $c, 1);
		$url       = mysql_result($res_c, $c, 2);
		$tipo      = mysql_result($res_c, $c, 3);
		$resposta .= "				<camera item='$x-2-$c' id='$id' active='0' url='$url' tipo='$tipo'> \n";
		$resposta .= "					<nome><![CDATA[$titulo]]></nome> \n";
		$resposta .= "					<imagens> \n";
		$sql_2     = "SELECT arquivo FROM camera_frames WHERE id_camera = $id ORDER BY id ASC";
		$res_2     = mysql_query($sql_2, $conn);
		if($res_2){
			$quantos_2 = mysql_num_rows($res_2);
			for($y=0; $y<$quantos_2; $y++){
				$arquivo = mysql_result($res_2, $y, 0);
				$caminho = "../frames/$arquivo";
				if(is_file($caminho)){
					$resposta .= "						<imagem arquivo='$arquivo' /> \n";
				}				
			}
		}
		$resposta .= "					</imagens> \n";
		$resposta .= "				</camera> \n";
	}
	$resposta .= "			</cameras> \n";
	$resposta .= "		</tag> \n";
	
	$resposta .= "	</scene> \n";	
	
}

$resposta .= "\n";
$resposta .= "</cidades_visiveis> \n";

print $resposta;

?>
