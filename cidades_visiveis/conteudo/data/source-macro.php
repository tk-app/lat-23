<?

include '../../connect.php';

$resposta  = '<?xml version="1.0" encoding="utf-8"?>';
$resposta .= "\n";
$resposta .= "\n";

$sql_m = "SELECT id FROM macros ORDER BY RAND() LIMIT 1";
$res_m = mysql_query($sql_m, $conn);
$macro = mysql_result($res_m, 0, 0);

$resposta .= "<cidades_visiveis macro='$macro'> \n";

$sql = "SELECT cena FROM macros_cena WHERE macro = $macro ORDER BY posicao ASC";
$res = mysql_query($sql, $conn);
$qts = mysql_num_rows($res);


for($x=0; $x<$qts; $x++){
	
	$cena    = mysql_result($res, $x, 0);	
	$sql_2   = "SELECT * FROM cenas WHERE id = $cena";
	$res_2   = mysql_query($sql_2, $conn);
	$cena_tt = mysql_result($res_2, 0, 1);
	$cena_dr = mysql_result($res_2, 0, 2);
	$cena_gd = mysql_result($res_2, 0, 3);	
	$cena_tr = mysql_result($res_2, 0, 10);
	
	$resposta .= "\n";
	$resposta .= "	<scene item='$x' cena='$cena' grid='$cena_gd' previous='' duracao='$cena_dr' corrido='0' trilha='$cena_tr'> \n";
	
	for($y=0; $y<6; $y++){
		
		$pont   = $y +4;
		$tag_id = mysql_result($res_2, 0, $pont);
		
		if($tag_id != 0){
			$sql_3  = "SELECT tag FROM tags WHERE id = $tag_id";
			$res_3  = mysql_query($sql_3, $conn);
			$qts_3  = mysql_num_rows($res_3);
			if($qts_3 > 0){
				$tag_nm = mysql_result($res_3, 0, 0);
			}else{
				$tag_nm = 'nenhuma';
			}			
		}else{
			$tag_nm = 'nenhuma';
		}
		
		
		$resposta .= "		<tag item='$y' id='$tag_id'> \n";
		$resposta .= "			<nome><![CDATA[$tag_nm]]></nome> \n";
		$resposta .= "			<cameras> \n";
		
		//camera
		if($tag_id != 0){
			$sql_c = "SELECT a.id, a.titulo, a.url, a.tipo, a.latitude, a.longitude
				      FROM cameras a, camera_tag b
				      WHERE a.publicado = 1
				      AND a.id = b.id_camera
				      AND b.id_tag = $tag_id
					  ORDER BY rand() 
					  LIMIT 1";

			$res_c = mysql_query($sql_c, $conn);
			$qts_c = mysql_num_rows($res_c);
			for($c=0; $c<$qts_c; $c++){
				$id        = mysql_result($res_c, $c, 0);
				$titulo    = mysql_result($res_c, $c, 1);
				$url       = mysql_result($res_c, $c, 2);
				$tipo      = mysql_result($res_c, $c, 3);
				$resposta .= "				<camera item='$x-1-$c' id='$id' active='0' url='$url' tipo='$tipo'> \n";
				$resposta .= "					<nome><![CDATA[$titulo]]></nome> \n";
				$resposta .= "					<imagens> \n";
				$sql_f     = "SELECT arquivo FROM camera_frames WHERE id_camera = $id ORDER BY id ASC";
				$res_f     = mysql_query($sql_f, $conn);
				if($res_f){
					$quantos_f = mysql_num_rows($res_f);
					for($k=0; $k<$quantos_f; $k++){
						$arquivo = mysql_result($res_f, $k, 0);
						$caminho = "../frames/$arquivo";
						if(is_file($caminho)){
							$resposta .= "						<imagem arquivo='$arquivo' /> \n";
						}
					}
				}
				$resposta .= "					</imagens> \n";
				$resposta .= "				</camera> \n";
			}
		}
		
		$resposta .= "			</cameras> \n";
		$resposta .= "		</tag> \n";
		
	}
	
	$resposta .= "	</scene> \n";
	
}

$resposta .= "\n";
$resposta .= "</cidades_visiveis> \n";

print $resposta;

?>
