<? 
 
	session_start();
	 
	if(isset($_SESSION["scaffold_id"])){ 
		$id = $_SESSION["scaffold_id"]; 
		if($id == 0 or $id == ""){ 
			header("Location: index.php"); 
		}else{ 
			include "connect.php";
		} 
	}else{ 
		$_SESSION["scaffold_id"] = ""; 
		header("Location: index.php"); 
	} 
 
?>

<html> 
<head> 
<title>bonacode scaffold</title> 
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"> 
<link rel="STYLESHEET" type="text/css" href="admin.css"> 
<script language="javascript" src="admin.js"></script> 
<script language="javascript" src="jquery-1.4.2.min.js"></script> 

<script language="javascript"> 

	function updateTipo(){
		var tipo = document.formulario.tipo.value;
		if(tipo == 1){			
			$('#tr-1').show();
			$('#tr-2').show();
			$('#tr-3').show();
			$('#tr-4').show();
			$('#tr-5').show();
			$('#tr-6').show();
			$('#opt-1').hide();
			$('#opt-2').show();
			$('#opt-3').show();
			$('#opt-4').show();
			$('#opt-5').show();
			$('#opt-6').show();
			$('#opt-7').show();
		}
		if(tipo == 2){
			$('#tr-1').show();
			$('#tr-2').hide();
			$('#tr-3').hide();
			$('#tr-4').show();
			$('#tr-5').show();
			$('#tr-6').hide();
			$('#opt-1').hide();
			$('#opt-2').hide();
			$('#opt-3').hide();
			$('#opt-4').show();
			$('#opt-5').show();
			$('#opt-6').hide();
		}
		if(tipo == 3){
			$('#tr-1').show();
			$('#tr-2').show();
			$('#tr-3').hide();
			$('#tr-4').show();
			$('#tr-5').hide();
			$('#tr-6').hide();
			$('#opt-1').show();
			$('#opt-2').show();
			$('#opt-3').hide();
			$('#opt-4').hide();
			$('#opt-5').hide();
			$('#opt-6').hide();
		}
	}

	function valida_form(){ 

		var titulo = document.formulario.titulo.value;
		if(titulo.length < 4){
			alert("prencha o campo título");
			return(false);
		}
		
		var duracao = document.formulario.duracao.value;
		if(duracao < 20 || duracao > 60){
			alert("a duração deve ter entre 20 e 60 segundos");
			return(false);
		}
		
		var tipo = document.formulario.tipo.value;
				
		if(tipo == 1 || tipo == 2){
			var tag_1 = document.formulario.tag_1.value;
			if(tag_1 == 0){
				alert("é necessário escolher uma tag - 1");
				return(false);
			}
		}
			
		
		if(tipo == 3){
			var tag_4 = document.formulario.tag_4.value;
			if(tag_4 == 0){
				alert("é necessário escolher uma tag - 4");
				return(false);
			}
		}
		

		if(trilha != "" && trilha.lastIndexOf(".mp3") == -1){
			alert("a trilha precisa ser um arquivo mp3");
			return(false);
		}

	} 

</script> 

</head> 
<body bgcolor="#ffffff"> 
 
<form method="POST" action="preview_cenas.php" name="formulario" id="formulario" enctype="multipart/form-data" onSubmit="return valida_form()"> 
<input type="hidden" name="id" value="0" /> 
<table border="0" cellpadding="3" cellspacing="3"> 
<tr> 
   <td width="75"></td> 
   <td width="300"></td> 
   <td width="150"></td> 
</tr> 
<tr> 
   <td></td> 
   <td colspan="2" class="titulo">NOVA CENA</td> 
</tr> 
<tr> 
  <td height="32" align="right" class="input_label">título</td> 
  <td><input type="text" name="titulo" size="40" /></td> 
  <td></td> 
</tr> 
<tr> 
  <td height="32" align="right" class="input_label">duracao</td> 
  <td><input type="text" name="duracao" size="40" /></td> 
  <td></td> 
</tr> 
<tr> 
  <td height="32" align="right" class="input_label">tipo</td> 
  <td>
	<select id="tipo" name="tipo" onchange="updateTipo()">
		<option value="1">6 cameras</option>
		<option value="2">1 grande / 1 ou 2 pequenas</option>
		<option value="3">1 ou 2 pequenas / 1 grande</option>
	</select>
  </td> 
  <td></td> 
</tr> 
<tr id="tr-1"> 
  <td height="32" align="right" class="input_label">tag 1</td> 
  <td> 
     <select id="tag_1" name="tag_1"> 
        <option value="0">escolha uma opção</option> 
		 <? 
			$sql = "SELECT id, tag FROM tags ORDER BY tag";
			$res = mysql_query($sql, $conn);
			if($res){
			   $quantos = mysql_num_rows($res);
			   for($x=0; $x<$quantos; $x++){
			      $op_id = mysql_result($res, $x, 0);
			      $op_nm = mysql_result($res, $x, 1);
				  echo"<option value=\"$op_id\">$op_nm</option> \n";
			   }
			}
		  ?> 
     </select> 
  </td> 
  <td><span id='opt-1'>opcional</span></td> 
</tr> 
<tr id="tr-2"> 
  <td height="32" align="right" class="input_label">tag 2</td> 
  <td> 
     <select id="tag_2" name="tag_2"> 
        <option value="0">escolha uma opção</option> 
		 <? 
			$sql = "SELECT id, tag FROM tags ORDER BY tag";
			$res = mysql_query($sql, $conn);
			if($res){
			   $quantos = mysql_num_rows($res);
			   for($x=0; $x<$quantos; $x++){
			      $op_id = mysql_result($res, $x, 0);
			      $op_nm = mysql_result($res, $x, 1);
				  echo"<option value=\"$op_id\">$op_nm</option> \n";
			   }
			}
		  ?> 
     </select> 
  </td> 
  <td><span id='opt-2'>opcional</span></td> 
</tr> 
<tr id="tr-3"> 
  <td height="32" align="right" class="input_label">tag 3</td> 
  <td> 
     <select id="tag_3" name="tag_3"> 
        <option value="0">escolha uma opção</option> 
		 <? 
			$sql = "SELECT id, tag FROM tags ORDER BY tag";
			$res = mysql_query($sql, $conn);
			if($res){
			   $quantos = mysql_num_rows($res);
			   for($x=0; $x<$quantos; $x++){
			      $op_id = mysql_result($res, $x, 0);
			      $op_nm = mysql_result($res, $x, 1);
				  echo"<option value=\"$op_id\">$op_nm</option> \n";
			   }
			}
		  ?> 
     </select> 
  </td> 
  <td><span id='opt-3'>opcional</span></td> 
</tr> 
<tr id="tr-4"> 
  <td height="32" align="right" class="input_label">tag 4</td> 
  <td> 
     <select id="tag_4" name="tag_4"> 
        <option value="0">escolha uma opção</option> 
		 <? 
			$sql = "SELECT id, tag FROM tags ORDER BY tag";
			$res = mysql_query($sql, $conn);
			if($res){
			   $quantos = mysql_num_rows($res);
			   for($x=0; $x<$quantos; $x++){
			      $op_id = mysql_result($res, $x, 0);
			      $op_nm = mysql_result($res, $x, 1);
				  echo"<option value=\"$op_id\">$op_nm</option> \n";
			   }
			}
		  ?> 
     </select> 
  </td> 
  <td><span id='opt-4'>opcional</span></td> 
</tr> 
<tr id="tr-5"> 
  <td height="32" align="right" class="input_label">tag 5</td> 
  <td> 
     <select id="tag_5" name="tag_5"> 
        <option value="0">escolha uma opção</option> 
		 <? 
			$sql = "SELECT id, tag FROM tags ORDER BY tag";
			$res = mysql_query($sql, $conn);
			if($res){
			   $quantos = mysql_num_rows($res);
			   for($x=0; $x<$quantos; $x++){
			      $op_id = mysql_result($res, $x, 0);
			      $op_nm = mysql_result($res, $x, 1);
				  echo"<option value=\"$op_id\">$op_nm</option> \n";
			   }
			}
		  ?> 
     </select> 
  </td> 
  <td><span id='opt-5'>opcional</span></td> 
</tr> 
<tr id="tr-6"> 
  <td height="32" align="right" class="input_label">tag 6</td> 
  <td> 
     <select id="tag_6" name="tag_6"> 
        <option value="0">escolha uma opção</option> 
		 <? 
			$sql = "SELECT id, tag FROM tags ORDER BY tag";
			$res = mysql_query($sql, $conn);
			if($res){
			   $quantos = mysql_num_rows($res);
			   for($x=0; $x<$quantos; $x++){
			      $op_id = mysql_result($res, $x, 0);
			      $op_nm = mysql_result($res, $x, 1);
				  echo"<option value=\"$op_id\">$op_nm</option> \n";
			   }
			}
		  ?> 
     </select> 
  </td> 
  <td><span id='opt-6'>opcional</span></td> 
</tr> 
<tr> 
  <td height="32" align="right" class="input_label">trilha</td> 
  <td><input type="file" name="trilha" size="30" /></td> 
  <td class="input_label">mp3 | 10240 kb</td> 
</tr> 
<tr><td></td><td></td><td></td></tr> 
<tr><td></td><td><input type="submit" value="CONTINUAR" /></td><td></td></tr> 
</table> 
</form> 

<script language="javascript"> 
	updateTipo();
</script>
 
</body> 
</html> 
