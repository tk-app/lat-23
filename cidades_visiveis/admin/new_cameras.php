<? 
 
	session_start();
	 
	if(isset($_SESSION["scaffold_id"])){ 
		$id = $_SESSION["scaffold_id"]; 
		if($id == 0 or $id == ""){ 
			header("Location: index.php"); 
		}else{ 
			include "connect.php";
		} 
	}else{ 
		$_SESSION["scaffold_id"] = ""; 
		header("Location: index.php"); 
	} 
 
?>

<html> 
<head> 
<title>Cidades Visíveis</title> 
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"> 
<link rel="STYLESHEET" type="text/css" href="admin.css"> 
<script language="javascript" src="admin.js"></script> 

<script language="javascript"> 

	function valida_form(){ 

		var titulo = document.formulario.titulo.value;
		if(titulo.length < 4){
			alert("prencha o campo título");
			return(false);
		}

		var url = document.formulario.url.value;
		if(url.length < 12){
			alert("prencha o campo url");
			return(false);
		}

	} 

</script> 

</head> 
<body bgcolor="#ffffff"> 
 
<form method="POST" action="preview_cameras.php" name="formulario" id="formulario" enctype="multipart/form-data" onSubmit="return valida_form()"> 
<input type="hidden" name="id" value="0" /> 
<table border="0" cellpadding="3" cellspacing="3"> 
<tr> 
   <td width="75"></td> 
   <td width="300"></td> 
   <td width="150"></td> 
</tr> 
<tr> 
   <td></td> 
   <td colspan="2" class="titulo">NOVA CAMERA</td> 
</tr> 
<tr> 
  <td height="32" align="right" class="input_label">título</td> 
  <td><input type="text" name="titulo" size="40" /></td> 
  <td></td> 
</tr> 
<tr> 
  <td height="32" align="right" class="input_label">url</td> 
  <td><input type="text" name="url" size="40" /></td> 
  <td></td> 
</tr> 
<tr> 
  <td height="32" align="right" class="input_label">tipo</td> 
  <td>
	<select name="tipo" name="tipo">
		<option value='jpg'>JPG</option>
		<option value='mjpg'>MJPG</option>
	</select>
  </td> 
  <td></td> 
</tr> 
<tr> 
  <td height="32" align="right" class="input_label">latitude</td> 
  <td><input type="text" name="latitude" size="40" /></td> 
  <td></td> 
</tr> 
<tr> 
  <td height="32" align="right" class="input_label">longitude</td> 
  <td><input type="text" name="longitude" size="40" /></td> 
  <td></td> 
</tr> 
<tr> 
  <td height="32" align="right" class="input_label">hemisferio</td> 
  <td>
	<select name="hemisferio" name="hemisferio">
		<option value='norte'>Norte</option>
		<option value='sul'>Sul</option>
	</select>
  </td> 
  <td></td> 
</tr> 

<tr> 
  <td height="32"></td> 
  <td class="input_label"> tags </td> 
  <td></td> 
</tr>

<?
	$sql_2 = "SELECT id, tag FROM tags ORDER BY tag ASC";
	$res_2 = mysql_query($sql_2, $conn);
	$qts_2 = mysql_num_rows($res_2);
	for($x=0; $x<$qts_2; $x++){
		$tag_id = mysql_result($res_2, $x, 0);
		$tag_nm = mysql_result($res_2, $x, 1);
?>

<tr> 
  <td height="32" align="right" >
  	<input type="checkbox" name="tags[]" value="<? echo $tag_id; ?>">
  </td> 
  <td class="input_value"><? echo $tag_nm; ?></td> 
  <td></td> 
</tr>

<?
	}
?>

<tr> 
  <td height="32" align="right" class="input_label">publicado</td> 
  <td class="input_value"> 
     <input type="radio" name="publicado" value="0" checked="checked" /> inativo | <input type="radio" name="publicado" value="1"  /> ativo 
  </td> 
  <td></td> 
</tr>
 
<tr><td></td><td></td><td></td></tr> 
<tr><td></td><td><input type="submit" value="CONTINUAR" /></td><td></td></tr> 
</table> 
</form> 
 
</body> 
</html> 
