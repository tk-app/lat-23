<? 
 
	session_start();
	 
	if(isset($_SESSION["scaffold_id"])){ 
		$id = $_SESSION["scaffold_id"]; 
		if($id == 0 or $id == ""){ 
			header("Location: index.php"); 
		}else{ 
			include "connect.php";
		} 
	}else{ 
		$_SESSION["scaffold_id"] = ""; 
		header("Location: index.php"); 
	} 
 
?>

<html> 
<head> 
<title>Cidades Visíveis</title> 
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"> 
<link rel="STYLESHEET" type="text/css" href="admin.css"> 
<script language="javascript" src="admin.js"></script> 

<script language="javascript"> 

	function valida_form(){ 

		var nome = document.formulario.nome.value;
		if(nome.length < 4){
			alert("o campo nome deve ter no mínimo 4 caracteres");
			return(false);
		}

		var email = document.formulario.email.value;
		if (mailcheck(email)==false){
			alert("digite um email válido");
			return(false);
		}

		var senha = document.formulario.senha.value;
		if(senha.length < 6){
			alert("digite sua senha [6 caracteres]");
			return(false);
		}

	} 

</script> 

</head> 
<body bgcolor="#ffffff"> 
 
<form method="POST" action="preview_editores.php" name="formulario" id="formulario" enctype="multipart/form-data" onSubmit="return valida_form()"> 
<input type="hidden" name="id" value="0" /> 
<table border="0" cellpadding="3" cellspacing="3"> 
<tr> 
   <td width="75"></td> 
   <td width="300"></td> 
   <td width="150"></td> 
</tr> 
<tr> 
   <td></td> 
   <td colspan="2" class="titulo">NOVO EDITOR</td> 
</tr> 
<tr> 
  <td height="32" align="right" class="input_label">nome</td> 
  <td><input type="text" name="nome" size="40" /></td> 
  <td></td> 
</tr> 
<tr> 
  <td height="32" align="right" class="input_label">email</td> 
  <td><input type="text" name="email" size="40" /></td> 
  <td></td> 
</tr> 
<tr> 
  <td height="32" align="right" class="input_label">senha</td> 
  <td><input type="password" name="senha" size="40" /></td> 
  <td></td> 
</tr> 
<tr><td></td><td></td><td></td></tr> 
<tr><td></td><td><input type="submit" value="CONTINUAR" /></td><td></td></tr> 
</table> 
</form> 
 
</body> 
</html> 
