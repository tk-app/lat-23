<? 
 
	session_start();
	 
	if(isset($_SESSION["scaffold_id"])){ 
		$id = $_SESSION["scaffold_id"]; 
		if($id == 0 or $id == ""){ 
			header("Location: index.php"); 
		}else{ 
			include "connect.php";
		} 
	}else{ 
		$_SESSION["scaffold_id"] = ""; 
		header("Location: index.php"); 
	} 
 
?>

<html> 
<head> 
<title>bonacode scaffold</title> 
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"> 
<link rel="STYLESHEET" type="text/css" href="admin.css"> 
<script language="javascript" src="admin.js"></script> 

<script language="javascript"> 

	function valida_form(){ 
		
		var titulo = document.formulario.titulo.value;
		if(titulo.length < 4){
			alert("prencha o campo título");
			return(false);
		}

		var tag_1 = document.formulario.tag_1.value;
		if(tag_1 == 0){
			alert("é necessário escolher uma tag - 1");
			return(false);
		}

		var tag_2 = document.formulario.tag_2.value;
		if(tag_2 == 0){
			alert("é necessário escolher uma tag - 2");
			return(false);
		}

		var tag_3 = document.formulario.tag_3.value;
		if(tag_3 == 0){
			alert("é necessário escolher uma tag - 3");
			return(false);
		}

		var tag_4 = document.formulario.tag_4.value;
		if(tag_4 == 0){
			alert("é necessário escolher uma tag - 3");
			return(false);
		}

		var tag_5 = document.formulario.tag_5.value;
		if(tag_5 == 0){
			alert("é necessário escolher uma tag - 4");
			return(false);
		}

	} 

</script> 

</head> 
<body bgcolor="#ffffff"> 
 
<?
 
	$id  = $_GET["id"];
	$sql = "SELECT * FROM sequencias WHERE id = $id";
	$res = mysql_query($sql, $conn);
	if($res){
		$quantos = mysql_num_rows($res);
		if($quantos == 1){
			$values = $res;
			$continua = true;
		}else{
			$continua = false;
		}
	}
 
	if($continua){
 
?>

<form method="POST" action="preview_update_sequencias.php" name="formulario" id="formulario" enctype="multipart/form-data" onSubmit="return valida_form()"> 
<input type="hidden" name="id" value="<? echo $id; ?>" /> 
<table border="0" cellpadding="3" cellspacing="3"> 
<tr> 
   <td width="75"></td> 
   <td width="300"></td> 
   <td width="150"></td> 
</tr> 
<tr> 
   <td></td> 
   <td colspan="2" class="titulo">EDITAR SEQUENCIA</td> 
</tr> 
  <? 
  	$value = mysql_result($values, 0, 1);
	$value = stripslashes($value); 
	$value = str_replace('"', '&quot;', $value); 
  ?> 

</tr> 
  <? 
  	$value = mysql_result($values, 0, 7);
	$value = stripslashes($value); 
	$value = str_replace('"', '&quot;', $value); 
  ?> 
<tr> 
  <td height="32" align="right" class="input_label">título</td> 
  <td><input type="text" name="titulo" size="40" value="<? echo $value; ?>" /></td> 
  <td></td> 
</tr>

  <? 
  	$value = mysql_result($values, 0, 2);
	$value = stripslashes($value); 
	$value = str_replace('"', '&quot;', $value); 
  ?> 
<tr> 
  <td height="32" align="right" class="input_label">tag / cam 1</td> 
  <td> 
     <select name="tag_1"> 
        <option value="0">escolha uma opção</option> 
		 <? 
			$sql = "SELECT id, tag FROM tags ORDER BY tag";
			$res = mysql_query($sql, $conn);
			if($res){
			   $quantos = mysql_num_rows($res);
			   for($x=0; $x<$quantos; $x++){
			      $op_id = mysql_result($res, $x, 0);
			      $op_nm = mysql_result($res, $x, 1);
			      if($op_id == $value){
			      	$selected="selected=\"selected\"";
			      }else{
			      	$selected="";
			      }
				  echo "<option value=\"$op_id\" $selected>$op_nm</option> \n";
			   }
			}
		  ?> 
     </select> 
  </td> 
  <td></td> 
</tr> 

<? 
  	$value = mysql_result($values, 0, 8);
	$value = stripslashes($value); 
	$value = str_replace('"', '&quot;', $value); 
  ?> 
<tr> 
  <td height="32" align="right" class="input_label">trilha 1</td> 
  <td class="input_value"><a href="../conteudo/mp3/<? echo $value; ?>" target="_blank"><? echo $value; ?></a></td> 
  <td></td> 
  <input type="hidden" name="trilha_1" value="<? echo $value; ?>" /> 
</tr> 
<tr> 
  <td height="32" align="right" class="input_label">alterar trilha ?</td> 
  <td><input type="file" name="new_trilha_1" size="40" /></td> 
  <td class="input_label">mp3 | 10240 kb</td> 
</tr>


  <? 
  	$value = mysql_result($values, 0, 3);
	$value = stripslashes($value); 
	$value = str_replace('"', '&quot;', $value); 
  ?> 
<tr> 
  <td height="32" align="right" class="input_label">tag / cam 2</td> 
  <td> 
     <select name="tag_2"> 
        <option value="0">escolha uma opção</option> 
		 <? 
			$sql = "SELECT id, tag FROM tags ORDER BY tag";
			$res = mysql_query($sql, $conn);
			if($res){
			   $quantos = mysql_num_rows($res);
			   for($x=0; $x<$quantos; $x++){
			      $op_id = mysql_result($res, $x, 0);
			      $op_nm = mysql_result($res, $x, 1);
			      if($op_id == $value){
			      	$selected="selected=\"selected\"";
			      }else{
			      	$selected="";
			      }
				  echo "<option value=\"$op_id\" $selected>$op_nm</option> \n";
			   }
			}
		  ?> 
     </select> 
  </td> 
  <td></td> 
</tr> 

<? 
  	$value = mysql_result($values, 0, 9);
	$value = stripslashes($value); 
	$value = str_replace('"', '&quot;', $value); 
  ?> 
<tr> 
  <td height="32" align="right" class="input_label">trilha 2</td> 
  <td class="input_value"><a href="../conteudo/mp3/<? echo $value; ?>" target="_blank"><? echo $value; ?></a></td> 
  <td></td> 
  <input type="hidden" name="trilha_2" value="<? echo $value; ?>" /> 
</tr> 
<tr> 
  <td height="32" align="right" class="input_label">alterar trilha ?</td> 
  <td><input type="file" name="new_trilha_2" size="40" /></td> 
  <td class="input_label">mp3 | 10240 kb</td> 
</tr>

  <? 
  	$value = mysql_result($values, 0, 4);
	$value = stripslashes($value); 
	$value = str_replace('"', '&quot;', $value); 
  ?> 
<tr> 
  <td height="32" align="right" class="input_label">tag / cam 3</td> 
  <td> 
     <select name="tag_3"> 
        <option value="0">escolha uma opção</option> 
		 <? 
			$sql = "SELECT id, tag FROM tags ORDER BY tag";
			$res = mysql_query($sql, $conn);
			if($res){
			   $quantos = mysql_num_rows($res);
			   for($x=0; $x<$quantos; $x++){
			      $op_id = mysql_result($res, $x, 0);
			      $op_nm = mysql_result($res, $x, 1);
			      if($op_id == $value){
			      	$selected="selected=\"selected\"";
			      }else{
			      	$selected="";
			      }
				  echo "<option value=\"$op_id\" $selected>$op_nm</option> \n";
			   }
			}
		  ?> 
     </select> 
  </td> 
  <td></td> 
</tr> 


<? 
  	$value = mysql_result($values, 0, 10);
	$value = stripslashes($value); 
	$value = str_replace('"', '&quot;', $value); 
  ?> 
<tr> 
  <td height="32" align="right" class="input_label">trilha 3</td> 
  <td class="input_value"><a href="../conteudo/mp3/<? echo $value; ?>" target="_blank"><? echo $value; ?></a></td> 
  <td></td> 
  <input type="hidden" name="trilha_3" value="<? echo $value; ?>" /> 
</tr> 
<tr> 
  <td height="32" align="right" class="input_label">alterar trilha ?</td> 
  <td><input type="file" name="new_trilha_3" size="40" /></td> 
  <td class="input_label">mp3 | 10240 kb</td> 
</tr>

  <? 
  	$value = mysql_result($values, 0, 5);
	$value = stripslashes($value); 
	$value = str_replace('"', '&quot;', $value); 
  ?> 
<tr> 
  <td height="32" align="right" class="input_label">tag / cam 4</td> 
  <td> 
     <select name="tag_4"> 
        <option value="0">escolha uma opção</option> 
		 <? 
			$sql = "SELECT id, tag FROM tags ORDER BY tag";
			$res = mysql_query($sql, $conn);
			if($res){
			   $quantos = mysql_num_rows($res);
			   for($x=0; $x<$quantos; $x++){
			      $op_id = mysql_result($res, $x, 0);
			      $op_nm = mysql_result($res, $x, 1);
			      if($op_id == $value){
			      	$selected="selected=\"selected\"";
			      }else{
			      	$selected="";
			      }
				  echo "<option value=\"$op_id\" $selected>$op_nm</option> \n";
			   }
			}
		  ?> 
     </select> 
  </td> 
  <td></td> 
</tr> 

<? 
  	$value = mysql_result($values, 0, 11);
	$value = stripslashes($value); 
	$value = str_replace('"', '&quot;', $value); 
  ?> 
<tr> 
  <td height="32" align="right" class="input_label">trilha 4</td> 
  <td class="input_value"><a href="../conteudo/mp3/<? echo $value; ?>" target="_blank"><? echo $value; ?></a></td> 
  <td></td> 
  <input type="hidden" name="trilha_4" value="<? echo $value; ?>" /> 
</tr> 
<tr> 
  <td height="32" align="right" class="input_label">alterar trilha ?</td> 
  <td><input type="file" name="new_trilha_4" size="40" /></td> 
  <td class="input_label">mp3 | 10240 kb</td> 
</tr>

  <? 
  	$value = mysql_result($values, 0, 6);
	$value = stripslashes($value); 
	$value = str_replace('"', '&quot;', $value); 
  ?> 
<tr> 
  <td height="32" align="right" class="input_label">tag / cam 5</td> 
  <td> 
     <select name="tag_5"> 
        <option value="0">escolha uma opção</option> 
		 <? 
			$sql = "SELECT id, tag FROM tags ORDER BY tag";
			$res = mysql_query($sql, $conn);
			if($res){
			   $quantos = mysql_num_rows($res);
			   for($x=0; $x<$quantos; $x++){
			      $op_id = mysql_result($res, $x, 0);
			      $op_nm = mysql_result($res, $x, 1);
			      if($op_id == $value){
			      	$selected="selected=\"selected\"";
			      }else{
			      	$selected="";
			      }
				  echo "<option value=\"$op_id\" $selected>$op_nm</option> \n";
			   }
			}
		  ?> 
     </select> 
  </td> 
  <td></td> 
</tr> 

<? 
  	$value = mysql_result($values, 0, 12);
	$value = stripslashes($value); 
	$value = str_replace('"', '&quot;', $value); 
  ?> 
<tr> 
  <td height="32" align="right" class="input_label">trilha 5</td> 
  <td class="input_value"><a href="../conteudo/mp3/<? echo $value; ?>" target="_blank"><? echo $value; ?></a></td> 
  <td></td> 
  <input type="hidden" name="trilha_5" value="<? echo $value; ?>" /> 
</tr> 
<tr> 
  <td height="32" align="right" class="input_label">alterar trilha ?</td> 
  <td><input type="file" name="new_trilha_5" size="40" /></td> 
  <td class="input_label">mp3 | 10240 kb</td> 
</tr>

<tr><td></td><td></td><td></td></tr> 
<tr><td></td><td><input type="submit" value="CONTINUAR" /></td><td></td></tr> 
</table> 
</form> 
<?
	}else{
		echo "<span class=\"titulo\">REGISTRO INVÁLIDO</span><br/><br/>";
  		echo "<a href=\"list_sequencias.php\">VOLTAR</a></br>";
	}
?>
 
</body> 
</html> 
