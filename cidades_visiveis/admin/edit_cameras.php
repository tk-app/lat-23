<? 
 
	session_start();
	 
	if(isset($_SESSION["scaffold_id"])){ 
		$id = $_SESSION["scaffold_id"]; 
		if($id == 0 or $id == ""){ 
			header("Location: index.php"); 
		}else{ 
			include "connect.php";
		} 
	}else{ 
		$_SESSION["scaffold_id"] = ""; 
		header("Location: index.php"); 
	} 
 
?>

<html> 
<head> 
<title>Cidades Visíveis</title> 
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"> 
<link rel="STYLESHEET" type="text/css" href="admin.css"> 
<script language="javascript" src="admin.js"></script> 

<script language="javascript"> 

	function valida_form(){ 

		var titulo = document.formulario.titulo.value;
		if(titulo.length < 4){
			alert("prencha o campo título");
			return(false);
		}

		var url = document.formulario.url.value;
		if(url.length < 12){
			alert("prencha o campo url");
			return(false);
		}

	} 

</script> 

</head> 
<body bgcolor="#ffffff"> 
 
<?
 
	$id  = $_GET["id"];
	$sql = "SELECT * FROM cameras WHERE id = $id";
	$res = mysql_query($sql, $conn);
	if($res){
		$quantos = mysql_num_rows($res);
		if($quantos == 1){
			$values = $res;
			$continua = true;
		}else{
			$continua = false;
		}
	}
 
	if($continua){
 
?>

<form method="POST" action="preview_update_cameras.php" name="formulario" id="formulario" enctype="multipart/form-data" onSubmit="return valida_form()"> 
<input type="hidden" name="id" value="<? echo $id; ?>" /> 
<table border="0" cellpadding="3" cellspacing="3"> 
<tr> 
   <td width="75"></td> 
   <td width="300"></td> 
   <td width="150"></td> 
</tr> 
<tr> 
   <td></td> 
   <td colspan="2" class="titulo">EDITAR CAMERA</td> 
</tr> 
  <? 
  	$value = mysql_result($values, 0, 1);
	$value = stripslashes($value); 
	$value = str_replace('"', '&quot;', $value); 
  ?> 
<tr> 
  <td height="32" align="right" class="input_label">título</td> 
  <td><input type="text" name="titulo" size="40" value="<? echo $value; ?>" /></td> 
  <td></td> 
</tr> 
  <? 
  	$value = mysql_result($values, 0, 2);
	$value = stripslashes($value); 
	$value = str_replace('"', '&quot;', $value); 
  ?> 
<tr> 
  <td height="32" align="right" class="input_label">url</td> 
  <td><input type="text" name="url" size="40" value="<? echo $value; ?>" /></td> 
  <td></td> 
</tr> 
  <? 
  	$value = mysql_result($values, 0, 3); 
	if($value == 'jpg'){
		$s1 = 'selected';
		$s2 = '';
	}else{
		$s2 = 'selected';
		$s1 = '';
	}
  ?> 
<tr> 
  <td height="32" align="right" class="input_label">tipo</td> 
  <td>
  	<select name="tipo" name="tipo">
		<option value='jpg' <? echo $s1; ?>>JPG</option>
		<option value='mjpg' <? echo $s2; ?>>MJPG</option>
	</select>
  </td> 
  <td></td> 
</tr> 
  <? 
  	$value = mysql_result($values, 0, 4);
	$value = stripslashes($value); 
	$value = str_replace('"', '&quot;', $value); 
  ?> 
<tr> 
  <td height="32" align="right" class="input_label">latitude</td> 
  <td><input type="text" name="latitude" size="40" value="<? echo $value; ?>" /></td> 
  <td></td> 
</tr> 
  <? 
  	$value = mysql_result($values, 0, 5);
	$value = stripslashes($value); 
	$value = str_replace('"', '&quot;', $value); 
  ?> 
<tr> 
  <td height="32" align="right" class="input_label">longitude</td> 
  <td><input type="text" name="longitude" size="40" value="<? echo $value; ?>" /></td> 
  <td></td> 
</tr> 
  <? 
  	$value = mysql_result($values, 0, 6);
	if($value == 'norte'){
		$s1 = 'selected';
		$s2 = '';
	}else{
		$s2 = 'selected';
		$s1 = '';
	}
  ?> 
<tr> 
  <td height="32" align="right" class="input_label">hemisferio</td> 
  <td>
	<select name="hemisferio" name="hemisferio">
		<option value='norte' <? echo $s1; ?>>Norte</option>
		<option value='sul' <? echo $s2; ?>>Sul</option>
	</select>
  </td> 
  <td></td> 
</tr> 

<tr> 
  <td height="32"></td> 
  <td class="input_label"> tags </td> 
  <td></td> 
</tr>

<?
	$sql_2 = "SELECT id, tag FROM tags ORDER BY tag ASC";
	$res_2 = mysql_query($sql_2, $conn);
	$qts_2 = mysql_num_rows($res_2);
	for($x=0; $x<$qts_2; $x++){
		$tag_id = mysql_result($res_2, $x, 0);
		$tag_nm = mysql_result($res_2, $x, 1);
		$sql_3  = "SELECT id FROM camera_tag WHERE id_camera = $id AND id_tag = $tag_id";
		$res_3 = mysql_query($sql_3, $conn);
		$qts_3 = mysql_num_rows($res_3);
		if($qts_3 == 0){
			$ck = '';
		}else{
			$ck = 'checked';
		}
?>

<tr> 
  <td height="32" align="right" >
  	<input type="checkbox" name="tags[]" value="<? echo $tag_id; ?>" <? echo $ck;?> >
  </td> 
  <td class="input_value"><? echo $tag_nm; ?></td> 
  <td></td> 
</tr>

<?
	}
?>

  <? 
  	$value = mysql_result($values, 0, 7);
	$value = stripslashes($value); 
	$value = str_replace('"', '&quot;', $value); 
  ?> 
<tr> 
  <td height="32" align="right" class="input_label">publicado</td> 
		 <? 
			if($value == 0){
			   $ck0 = "checked=\"checked\"";
			   $ck1 = "";
			}else if($value == 1){
			   $ck0 = "";
			   $ck1 = "checked=\"checked\"";
			}
		 ?> 
  <td class="input_value"> 
     <input type="radio" name="publicado" value="0" <? echo $ck0; ?>/> inativo | <input type="radio" name="publicado" value="1" <? echo $ck1; ?>/> ativo 
  </td> 
  <td></td> 
</tr> 
<tr><td></td><td></td><td></td></tr> 
<tr><td></td><td><input type="submit" value="CONTINUAR" /></td><td></td></tr> 
</table> 
</form> 
<?
	}else{
		echo "<span class=\"titulo\">REGISTRO INVÁLIDO</span><br/><br/>";
  		echo "<a href=\"list_cameras.php\">VOLTAR</a></br>";
	}
?>
 
</body> 
</html> 
