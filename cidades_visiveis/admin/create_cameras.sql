-- create_table_cameras

DROP TABLE IF EXISTS cameras; 
CREATE TABLE cameras ( 
   id INT(11) NOT NULL AUTO_INCREMENT, 
   titulo VARCHAR(255) NOT NULL, 
   url VARCHAR(255) NOT NULL, 
   tipo VARCHAR(255) NOT NULL, 
   latitude VARCHAR(255) NULL, 
   longitude VARCHAR(255) NULL, 
   hemisferio VARCHAR(255) NULL, 
   publicado SMALLINT NOT NULL, 
   date_created TIMESTAMP NULL DEFAULT NULL, 
   date_updated TIMESTAMP NULL DEFAULT NULL, 
   PRIMARY KEY (id) 
) 
ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1; 
