<? 

$dir   = $_GET['dir'];
$nome  = $_GET['nome'];
$new_w = $_GET['new_w'];
$new_h = $_GET['new_h'];

$tipo  = explode('.', $nome);
$ext   = $tipo[1];

$file = $dir.'/'.$nome;

$size = getimagesize($file);

$width = $size[0]; 
$height = $size[1]; 

if($new_h == "null"){
	$new_h = ($new_w * $height)/ $width;
}else if($new_w == "null"){
	$new_w = ($new_h * $width)/ $height;
}

$dst_img = imagecreatetruecolor($new_w,$new_h);

if($ext == 'jpg'){
	$src_img = imagecreatefromjpeg("$file");
}else if($ext == 'png'){
	$src_img = imagecreatefrompng("$file");
}

ImageCopyResampled($dst_img, $src_img, 0, 0, 0, 0, $new_w, $new_h, $width, $height); 

if($ext == 'jpg'){
	header ("Content-type: image/jpeg");
	imagejpeg($dst_img, '', 100);
	imagedestroy ($dst_img);
}else if($ext == 'png'){
	header ("Content-type: image/png");
	imagepng($dst_img, NULL, 0, NULL);
	imagedestroy ($dst_img);
}

?> 