﻿
function loadCSS(target_mc:MovieClip, css_file_name:String) {
	if (!target_mc) {
		target_mc = _root;
	}
	//trace(css_file_name);
	target_mc.myCSS = new TextField.StyleSheet();
	target_mc.myCSS.onLoad = function() {
		target_mc.onEnterFrame = null;
		loadCSSComplete();
	};
	target_mc.onEnterFrame = function() {
		c = target_mc.myCSS.getBytesLoaded();
		t = target_mc.myCSS.getBytesTotal();
		p = int((c*100)/t);
		if (p>0) {
			//loadCSSProgress(p);
			//trace(p);
		}
		if (p == 100) {
			target_mc.onEnterFrame = null;
		}
	};
	target_mc.myCSS.load(css_file_name);
}