﻿function scrollMcOnBtnH(mask, alvo, onde){
	horizonte = mask._y; //alvo._y;
	area      = mask._height;
	altura    = alvo._height;
	delta_h   = altura - area;
	//delta_h   += 15;
	limite    = delta_h * -1;
	nivel     = onde.getNextHighestDepth();
	X         = mask._x + mask._width + 5;
	Y         = mask._y;
	W         = 10;
	H         = mask._height;
	if(delta_h < H){
		drag_h = H - delta_h;
	}else{
		drag_h = 15;
	}
	if(drag_h < 15){
		//drag_h = 15;
	}	
	alcance = (H - drag_h);
	step = (delta_h / alcance);
	step = Math.ceil(step);
	//delta inicial
	delta_inicial = mask._y - alvo._y
	delta_inicial = int(delta_inicial / step);
	if(delta_inicial > alcance){
		diff = delta_inicial - alcance;
		delta_inicial -= diff;
	}
	onde.scroll_line.removeMovieClip();
	onde.scroll_btn.removeMovieClip();
	drawBoxFill(onde, 'scroll_line',X, Y, 4, H, 100);
	drawBoxFill(onde, 'scroll_btn',X, (Y + delta_inicial), 4, drag_h, 100);
	my_color = new Color(onde.scroll_line);
	my_color.setRGB(0x000000);
	my_color = new Color(onde.scroll_btn);
	my_color.setRGB(0xff0000);
	onde.scroll_btn.centro = X;
	onde.scroll_btn.top_l = Y;
	onde.scroll_btn.botton_l = Y + alcance;
	onde.scroll_btn.alcance = alcance;
	onde.scroll_btn.step = step;
	onde.scroll_btn.onPress = function(){
		startDrag(this, false, this.centro, this.top_l, this.centro, this.botton_l);
		onde.step = this.step;
		onde.scroll_line.onEnterFrame = function(){
			percorrido = onde.scroll_btn._y - onde.scroll_line._y;
			atual      = Math.ceil(percorrido * onde.step);
			alvo.new_y = horizonte - atual;
			alvo._y    = alvo.new_y;
		}
	}
	onde.scroll_btn.onRelease = function(){
		stopDrag();
		onde.scroll_line.onEnterFrame = null;
	}
	onde.scroll_btn.onReleaseOutside = function(){
		stopDrag();
		onde.scroll_line.onEnterFrame = null;
	}
	
	percorrido = onde.scroll_btn._y - onde.scroll_line._y;
	atual      = Math.ceil(percorrido * step);
	alvo.new_y = horizonte - atual;
	alvo._y    = alvo.new_y;
	
}