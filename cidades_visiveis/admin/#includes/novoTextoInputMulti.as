﻿function novoTextoInputMulti(alvo,nome,posX,posY,W,H, pass){
	var onde:Object = new Object;
	onde = alvo;
	nivel = onde.getNextHighestDepth();
	onde.createTextField(nome,nivel,posX,posY,W,H);
	onde[nome].autoSize = false;
	onde[nome].wordWrap = true;
	onde[nome].multiline = true;
	onde[nome].border = false;	
	onde[nome].type = "INPUT";
	if(pass == 1){
		onde[nome].password = true;
	}
	onde[nome].onChanged = function(){
		setaFormatoTexto(onde[nome]);
	}	
}
