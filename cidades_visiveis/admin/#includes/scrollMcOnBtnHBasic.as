﻿function scrollMcOnBtnHBasic(mask, alvo, onde){
	horizonte = mask._y;
	area = mask._height;
	altura = alvo._height;
	delta_h = altura - area;
	delta_h += 15;
	limite = delta_h * -1;
	nivel = onde.getNextHighestDepth();
	X = mask._x + mask._width + 2;
	Y = mask._y;
	W = 15;
	H = mask._height;
	//contas iniciais
	if(delta_h < H){
		drag_h = H - delta_h;
		grande = false;
	}else{
		drag_h = 15;
		grande = true;
	}
	if(drag_h < 15){
		drag_h = 15;
	}
	alcance = (H - drag_h);
	step = (delta_h / alcance);
	//delta inicial
	delta_inicial = mask._y - alvo._y
	delta_inicial = int(delta_inicial / step);
	if(delta_inicial > alcance){
		diff = delta_inicial - alcance;
		delta_inicial -= diff;
	}
	//cria o grafico
	drawBoxFill(onde, 'scroll_box_btn',X, Y, W, H, 20);
	drawBoxLine(onde, 'scroll_box',X, Y, W, H, 75);
	drawBoxFill(onde, 'scroll_line',X + 7, Y, 1, H, 20);
	drawBoxFill(onde, 'scroll_btn',X, (Y + delta_inicial), 15, drag_h, 100);	
	my_color = new Color(onde.scroll_box);
	my_color.setRGB(0xd8d8d8);
	my_color = new Color(onde.scroll_line);
	my_color.setRGB(0xd8d8d8);
	my_color = new Color(onde.scroll_btn);
	my_color.setRGB(0xd8d8d8);
	my_color = new Color(onde.scroll_box_btn);
	my_color.setRGB(0xd8d8d8);	
	onde.scroll_box_btn.onRelease = function(){
		mouse_y = onde._ymouse;
		if(mouse_y < onde.scroll_btn._y + onde.scroll_btn._height){
			upScrollBtn();
		}else{
			downScrollBtn();
		}
	}
	function upScrollBtn(){
		_root.dragando = true;
		falta = int(onde.scroll_btn._y - onde.scroll_btn.top_l);
		if(falta > 50){
			falta = int(falta/2);
		}
		onde.scroll_btn._y -= falta;
		percorrido = onde.scroll_btn._y - onde.scroll_line._y;
		atual = Math.round(percorrido * step);
		alvo._y = horizonte - atual;
	}
	function downScrollBtn(){
		_root.dragando = true;
		falta = int(onde.scroll_btn.botton_l - onde.scroll_btn._y);
		if(falta > 50){
			falta = int(falta/2);
		}
		onde.scroll_btn._y += falta;
		percorrido = onde.scroll_btn._y - onde.scroll_line._y;
		atual = Math.round(percorrido * step);
		alvo._y = horizonte - atual;
	}
	onde.scroll_btn.centro = X;
	onde.scroll_btn.top_l = Y;
	onde.scroll_btn.botton_l = Y + alcance;
	onde.scroll_line.step = step;
	onde.scroll_btn.onPress = function(){
		_root.dragando = true;
		startDrag(this, false, this.centro, this.top_l, this.centro, this.botton_l);
		onde.scroll_line.onEnterFrame = function(){
			percorrido = onde.scroll_btn._y - onde.scroll_line._y;
			atual = Math.round(percorrido * this.step);
			alvo._y = horizonte - atual;
		}
	}
	onde.scroll_btn.onRelease = function(){
		_root.dragando = false;
		stopDrag();
		onde.scroll_line.onEnterFrame = null;
	}
	onde.scroll_btn.onReleaseOutside = function(){
		_root.dragando = false;
		stopDrag();
		onde.scroll_line.onEnterFrame = null;
	}
}