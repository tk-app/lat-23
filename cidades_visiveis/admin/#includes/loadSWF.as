﻿
function loadSWF(target_mc:MovieClip, file_url:String) {
	//trace(target_mc);
	//trace(file_url);
	target_mc.file_url = file_url;
	target_mc.loader_mc = new MovieClipLoader();
	target_mc.myListener = new Object();
	target_mc.myListener.onLoadStart = function() {
		//trace("download iniciou");
	};
	target_mc.myListener.onLoadComplete = function() {
		//trace("download concluido");
	};
	target_mc.myListener.onLoadInit = function() {
		//trace("imagem pronta para ser exibida");
		//trace(target_mc._parent);
		imageDisplay(target_mc);
		//target_mc._parent.loadSWFComplete();
	};
	target_mc.myListener.onLoadProgress = function(target:MovieClip, bytesLoaded:Number, bytesTotal:Number):Void  {
		target_mc.t = bytesTotal;
		target_mc.c = bytesLoaded;
		target_mc.p = int((target_mc.c*100)/target_mc.t);
		//target_mc.loadSWFProgress(target_mc.p);
		//trace(target_mc.p);
	};
	myListener.onLoadError = function(target_mc:MovieClip, errorCode:String, httpStatus:Number) {
		erro_msg = "'HTTP Status: "+httpStatus+" - "+errorCode+": "+file_url+"'";
		getURL("javascript:alert("+erro_msg+")");
		trace(">> Erro: "+errorCode);
		trace(">> httpStatus: "+httpStatus);
	};
	target_mc.loader_mc.addListener(target_mc.myListener);
	target_mc.loader_mc.loadClip(target_mc.file_url, target_mc);
}
//
function imageDisplay(target_mc) {
	target_mc._parent.loadSWFComplete();
}
