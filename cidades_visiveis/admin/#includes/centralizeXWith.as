﻿
function centralizeXWith(target_mc:MovieClip, target2_mc, x_tween:Boolean) {
	trace(target_mc);
	trace(target2_mc);
	target_mc.x_tween = x_tween;
	if (target2_mc._name) {
		target2_center_x = Math.ceil(target2_mc._width/2);
	} else if (target2_mc == "stage_width") {
		target2_center_x = Math.ceil(Stage.width/2);
	} else {
		target2_center_x = target2_mc;
	}
	target_mc.novo_x = Math.ceil((target2_center_x)-(target_mc._width/2));
	if (target_mc.x_tween) {
		moveX(target_mc, target_mc.novo_x);
	} else {
		target_mc._x = target_mc.novo_x;
	}
}
