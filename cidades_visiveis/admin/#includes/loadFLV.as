﻿
function loadFLV(target_mc:MovieClip, file_name:String, video_width:Number, video_height:Number) {

	target_mc.file_name = file_name;
	target_mc.video_width = video_width;
	target_mc.video_height = video_height;

	new_level = target_mc.getNextHighestDepth();
	target_mc.FLVPlayback_mc.removeMovieClip();
	if (!target_mc.FLVPlayback_mc) {
		target_mc.attachMovie("FLVPlayback", "FLVPlayback_mc", new_level, {_width:video_width, _height:video_height, _x:0, _y:0});
	}
  
	target_mc.FLVPlayback_mc._alpha = 0;
	target_mc.FLVPlayback_mc.skin = "ClearOverAll.swf";
	target_mc.FLVPlayback_mc.skinAutoHide = true;
	target_mc.FLVPlayback_mc.autoPlay = true;
	target_mc.FLVPlayback_mc.contentPath = target_mc.file_name;
	target_mc.FLVPlayback_mc.autoRewind = false;
	target_mc.videoListener = new Object();
	target_mc.videoListener.stateChange = function(eventObject:Object):Void  {
		if (eventObject.state == "playing") {
			target_mc._parent.loadFLVComplete();
			alphaTween(target_mc.FLVPlayback_mc, 100);
		}
	};
	target_mc.FLVPlayback_mc.addEventListener("stateChange", target_mc.videoListener);
	target_mc.videoListener.cuePoint = function(eventObject:Object):Void  {
		if (eventObject.info.name == "Finished" or eventObject.info.name == "Finish") {
			if (target_mc._parent._parent.playlist_autoplay == true) {
				target_mc._parent._parent.actual_video_node++;
				updateVideoContent(target_mc._parent, video_playlist_xml);
			}
		}
	};
	target_mc.FLVPlayback_mc.addEventListener("cuePoint", target_mc.videoListener);
}

