﻿
function scaleHeight(target_mc:MovieClip, scaleHeight_yscale_finish:Number, scaleHeight_callback:Boolean, scaleHeight_yscale_start:Number, scaleHeight_tween_type:String, scaleHeight_tween_speed:Number) {
	if (scaleHeight_yscale_start == undefined) {
		scaleHeight_yscale_start = target_mc._yscale;
	}
	//                 
	if (scaleHeight_tween_type == undefined) {
		scaleHeight_tween_type = "Strong.easeInOut";
	}
	//                        
	if (scaleHeight_tween_speed == undefined) {
		scaleHeight_tween_speed = 0.6;
	}
	//                           
	target_mc.scaleHeight_yscale_start = scaleHeight_yscale_start;
	target_mc.scaleHeight_yscale_finish = scaleHeight_yscale_finish;
	target_mc.scaleHeight_callback = scaleHeight_callback;
	target_mc.scaleHeight_tween_speed = scaleHeight_tween_speed;
	target_mc.scaleHeight_tween_type = scaleHeight_tween_type;
	//
	if (target_mc.scaleHeight_tween_type == 'Elastic.easeIn') {
		target_mc.scaleHeight_tween = new Tween(target_mc, "_yscale", Elastic.easeIn, target_mc.scaleHeight_yscale_start, target_mc.scaleHeight_yscale_finish, target_mc.scaleHeight_tween_speed, true);
	} else if (target_mc.scaleHeight_tween_type == 'Elastic.easeOut') {
		target_mc.scaleHeight_tween = new Tween(target_mc, "_yscale", ELastic.easeOut, target_mc.scaleHeight_yscale_start, target_mc.scaleHeight_yscale_finish, target_mc.scaleHeight_tween_speed, true);
	} else if (target_mc.scaleHeight_tween_type == 'Elastic.easeInOut') {
		target_mc.scaleHeight_tween = new Tween(target_mc, "_yscale", Elastic.easeInOut, target_mc.scaleHeight_yscale_start, target_mc.scaleHeight_yscale_finish, target_mc.scaleHeight_tween_speed, true);
	} else if (target_mc.scaleHeight_tween_type == 'Strong.easeIn') {
		target_mc.scaleHeight_tween = new Tween(target_mc, "_yscale", Strong.easeIn, target_mc.scaleHeight_yscale_start, target_mc.scaleHeight_yscale_finish, target_mc.scaleHeight_tween_speed, true);
	} else if (target_mc.scaleHeight_tween_type == 'Strong.easeOut') {
		target_mc.scaleHeight_tween = new Tween(target_mc, "_yscale", Strong.easeOut, target_mc.scaleHeight_yscale_start, target_mc.scaleHeight_yscale_finish, target_mc.scaleHeight_tween_speed, true);
	} else if (target_mc.scaleHeight_tween_type == 'Strong.easeInOut') {
		target_mc.scaleHeight_tween = new Tween(target_mc, "_yscale", Strong.easeInOut, target_mc.scaleHeight_yscale_start, target_mc.scaleHeight_yscale_finish, target_mc.scaleHeight_tween_speed, true);
	} else if (target_mc.scaleHeight_tween_type == 'Back.easeIn') {
		target_mc.scaleHeight_tween = new Tween(target_mc, "_yscale", Back.easeIn, target_mc.scaleHeight_yscale_start, target_mc.scaleHeight_yscale_finish, target_mc.scaleHeight_tween_speed, true);
	} else if (target_mc.scaleHeight_tween_type == 'Back.easeOut') {
		target_mc.scaleHeight_tween = new Tween(target_mc, "_yscale", Back.easeOut, target_mc.scaleHeight_yscale_start, target_mc.scaleHeight_yscale_finish, target_mc.scaleHeight_tween_speed, true);
	} else if (target_mc.scaleHeight_tween_type == 'Back.easeInOut') {
		target_mc.scaleHeight_tween = new Tween(target_mc, "_yscale", Back.easeInOut, target_mc.scaleHeight_yscale_start, target_mc.scaleHeight_yscale_finish, target_mc.scaleHeight_tween_speed, true);
	} else if (target_mc.scaleHeight_tween_type == 'Regular.easeIn') {
		target_mc.scaleHeight_tween = new Tween(target_mc, "_yscale", Regular.easeIn, target_mc.scaleHeight_yscale_start, target_mc.scaleHeight_yscale_finish, target_mc.scaleHeight_tween_speed, true);
	} else if (target_mc.scaleHeight_tween_type == 'Regular.easeOut') {
		target_mc.scaleHeight_tween = new Tween(target_mc, "_yscale", Regular.easeOut, target_mc.scaleHeight_yscale_start, target_mc.scaleHeight_yscale_finish, target_mc.scaleHeight_tween_speed, true);
	} else if (target_mc.scaleHeight_tween_type == 'Regular.easeInOut') {
		target_mc.scaleHeight_tween = new Tween(target_mc, "_yscale", Regular.easeInOut, target_mc.scaleHeight_yscale_start, target_mc.scaleHeight_yscale_finish, target_mc.scaleHeight_tween_speed, true);
	} else {
		//   
		//
		target_mc.scaleHeight_tween.onMotionChanged = function() {
		};
	}
	//
	target_mc.scaleHeight_tween.onMotionStopped = function() {
		if (target_mc.scaleHeight_callback) {
			target_mc.scaleHeightComplete();
		}
	};
}
//
