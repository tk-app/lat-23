﻿
function createText(target_mc:MovieClip, txt_name:String, txt_x:Number, txt_y:Number, txt_width:Number, txt_height:Number, txt_selectable:Boolean, txt_border:Boolean, txt_anti_alias:Boolean) {
	//trace(target_mc);
	//trace(txt_name);
	//trace(txt_x);
	txt_level = target_mc.getNextHighestDepth();
	target_mc.createTextField(txt_name, txt_level, txt_x, txt_y, txt_width, txt_height);
	//target_mc[txt_name]._alpha = 0;
	//target_mc[txt_name]._visible = false;
	target_mc[txt_name].autoSize = true;
	target_mc[txt_name].wordWrap = false;
	target_mc[txt_name].multiline = false;
	if (txt_border) {
		target_mc[txt_name].border = true;
	} else {
		target_mc[txt_name].border = false;
	}
	if (txt_selectable) {
		target_mc[txt_name].selectable = true;
	} else {
		target_mc[txt_name].selectable = false;
	}
	if (txt_anti_alias) {
		target_mc[txt_name].antiAliasType = 'advanced';
		target_mc[txt_name].sharpness = -264;
		target_mc[txt_name].thickness = 88;
		target_mc[txt_name].gridFitType = "pixel";
	}
}