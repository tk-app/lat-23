﻿
function resizeHeight(target_mc:MovieClip, resizeHeight_height_finish:Number, resizeHeight_callback:Boolean, resizeHeight_height_start:Number, resizeHeight_tween_type:String, resizeHeight_tween_speed:Number) {
	if (resizeHeight_height_start == undefined) {
		resizeHeight_height_start = target_mc._height;
	}
	//                 
	if (resizeHeight_tween_type == undefined) {
		resizeHeight_tween_type = "Strong.easeInOut";
	}
	//                        
	if (resizeHeight_tween_speed == undefined) {
		resizeHeight_tween_speed = 0.6;
	}
	//                           
	target_mc.resizeHeight_height_start = resizeHeight_height_start;
	target_mc.resizeHeight_height_finish = resizeHeight_height_finish;
	target_mc.resizeHeight_callback = resizeHeight_callback;
	target_mc.resizeHeight_tween_speed = resizeHeight_tween_speed;
	target_mc.resizeHeight_tween_type = resizeHeight_tween_type;
	//
	if (target_mc.resizeHeight_tween_type == 'Elastic.easeIn') {
		target_mc.resizeHeight_tween = new Tween(target_mc, "_height", Elastic.easeIn, target_mc.resizeHeight_height_start, target_mc.resizeHeight_height_finish, target_mc.resizeHeight_tween_speed, true);
	} else if (target_mc.resizeHeight_tween_type == 'Elastic.easeOut') {
		target_mc.resizeHeight_tween = new Tween(target_mc, "_height", ELastic.easeOut, target_mc.resizeHeight_height_start, target_mc.resizeHeight_height_finish, target_mc.resizeHeight_tween_speed, true);
	} else if (target_mc.resizeHeight_tween_type == 'Elastic.easeInOut') {
		target_mc.resizeHeight_tween = new Tween(target_mc, "_height", Elastic.easeInOut, target_mc.resizeHeight_height_start, target_mc.resizeHeight_height_finish, target_mc.resizeHeight_tween_speed, true);
	} else if (target_mc.resizeHeight_tween_type == 'Strong.easeIn') {
		target_mc.resizeHeight_tween = new Tween(target_mc, "_height", Strong.easeIn, target_mc.resizeHeight_height_start, target_mc.resizeHeight_height_finish, target_mc.resizeHeight_tween_speed, true);
	} else if (target_mc.resizeHeight_tween_type == 'Strong.easeOut') {
		target_mc.resizeHeight_tween = new Tween(target_mc, "_height", Strong.easeOut, target_mc.resizeHeight_height_start, target_mc.resizeHeight_height_finish, target_mc.resizeHeight_tween_speed, true);
	} else if (target_mc.resizeHeight_tween_type == 'Strong.easeInOut') {
		target_mc.resizeHeight_tween = new Tween(target_mc, "_height", Strong.easeInOut, target_mc.resizeHeight_height_start, target_mc.resizeHeight_height_finish, target_mc.resizeHeight_tween_speed, true);
	} else if (target_mc.resizeHeight_tween_type == 'Back.easeIn') {
		target_mc.resizeHeight_tween = new Tween(target_mc, "_height", Back.easeIn, target_mc.resizeHeight_height_start, target_mc.resizeHeight_height_finish, target_mc.resizeHeight_tween_speed, true);
	} else if (target_mc.resizeHeight_tween_type == 'Back.easeOut') {
		target_mc.resizeHeight_tween = new Tween(target_mc, "_height", Back.easeOut, target_mc.resizeHeight_height_start, target_mc.resizeHeight_height_finish, target_mc.resizeHeight_tween_speed, true);
	} else if (target_mc.resizeHeight_tween_type == 'Back.easeInOut') {
		target_mc.resizeHeight_tween = new Tween(target_mc, "_height", Back.easeInOut, target_mc.resizeHeight_height_start, target_mc.resizeHeight_height_finish, target_mc.resizeHeight_tween_speed, true);
	} else if (target_mc.resizeHeight_tween_type == 'Regular.easeIn') {
		target_mc.resizeHeight_tween = new Tween(target_mc, "_height", Regular.easeIn, target_mc.resizeHeight_height_start, target_mc.resizeHeight_height_finish, target_mc.resizeHeight_tween_speed, true);
	} else if (target_mc.resizeHeight_tween_type == 'Regular.easeOut') {
		target_mc.resizeHeight_tween = new Tween(target_mc, "_height", Regular.easeOut, target_mc.resizeHeight_height_start, target_mc.resizeHeight_height_finish, target_mc.resizeHeight_tween_speed, true);
	} else if (target_mc.resizeHeight_tween_type == 'Regular.easeInOut') {
		target_mc.resizeHeight_tween = new Tween(target_mc, "_height", Regular.easeInOut, target_mc.resizeHeight_height_start, target_mc.resizeHeight_height_finish, target_mc.resizeHeight_tween_speed, true);
	} else {
		//   
		//
		target_mc.resizeHeight_tween.onMotionChanged = function() {
		};
	}
	//
	target_mc.resizeHeight_tween.onMotionStopped = function() {
		if (target_mc.resizeHeight_callback) {
			target_mc.resizeHeightComplete();
		}
	};
}
//
