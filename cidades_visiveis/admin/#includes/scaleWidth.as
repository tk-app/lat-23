﻿
function scaleWidth(target_mc:MovieClip, scaleWidth_xscale_finish:Number, scaleWidth_callback:Boolean, scaleWidth_xscale_start:Number, scaleWidth_tween_type:String, scaleWidth_tween_speed:Number) {
	if (scaleWidth_xscale_start == undefined) {
		scaleWidth_xscale_start = target_mc._xscale;
	}
	//                 
	if (scaleWidth_tween_type == undefined) {
		scaleWidth_tween_type = "Strong.easeInOut";
	}
	//                        
	if (scaleWidth_tween_speed == undefined) {
		scaleWidth_tween_speed = 0.6;
	}
	//                           
	target_mc.scaleWidth_xscale_start = scaleWidth_xscale_start;
	target_mc.scaleWidth_xscale_finish = scaleWidth_xscale_finish;
	target_mc.scaleWidth_callback = scaleWidth_callback;
	target_mc.scaleWidth_tween_speed = scaleWidth_tween_speed;
	target_mc.scaleWidth_tween_type = scaleWidth_tween_type;
	//
	if (target_mc.scaleWidth_tween_type == 'Elastic.easeIn') {
		target_mc.scaleWidth_tween = new Tween(target_mc, "_xscale", Elastic.easeIn, target_mc.scaleWidth_xscale_start, target_mc.scaleWidth_xscale_finish, target_mc.scaleWidth_tween_speed, true);
	} else if (target_mc.scaleWidth_tween_type == 'Elastic.easeOut') {
		target_mc.scaleWidth_tween = new Tween(target_mc, "_xscale", ELastic.easeOut, target_mc.scaleWidth_xscale_start, target_mc.scaleWidth_xscale_finish, target_mc.scaleWidth_tween_speed, true);
	} else if (target_mc.scaleWidth_tween_type == 'Elastic.easeInOut') {
		target_mc.scaleWidth_tween = new Tween(target_mc, "_xscale", Elastic.easeInOut, target_mc.scaleWidth_xscale_start, target_mc.scaleWidth_xscale_finish, target_mc.scaleWidth_tween_speed, true);
	} else if (target_mc.scaleWidth_tween_type == 'Strong.easeIn') {
		target_mc.scaleWidth_tween = new Tween(target_mc, "_xscale", Strong.easeIn, target_mc.scaleWidth_xscale_start, target_mc.scaleWidth_xscale_finish, target_mc.scaleWidth_tween_speed, true);
	} else if (target_mc.scaleWidth_tween_type == 'Strong.easeOut') {
		target_mc.scaleWidth_tween = new Tween(target_mc, "_xscale", Strong.easeOut, target_mc.scaleWidth_xscale_start, target_mc.scaleWidth_xscale_finish, target_mc.scaleWidth_tween_speed, true);
	} else if (target_mc.scaleWidth_tween_type == 'Strong.easeInOut') {
		target_mc.scaleWidth_tween = new Tween(target_mc, "_xscale", Strong.easeInOut, target_mc.scaleWidth_xscale_start, target_mc.scaleWidth_xscale_finish, target_mc.scaleWidth_tween_speed, true);
	} else if (target_mc.scaleWidth_tween_type == 'Back.easeIn') {
		target_mc.scaleWidth_tween = new Tween(target_mc, "_xscale", Back.easeIn, target_mc.scaleWidth_xscale_start, target_mc.scaleWidth_xscale_finish, target_mc.scaleWidth_tween_speed, true);
	} else if (target_mc.scaleWidth_tween_type == 'Back.easeOut') {
		target_mc.scaleWidth_tween = new Tween(target_mc, "_xscale", Back.easeOut, target_mc.scaleWidth_xscale_start, target_mc.scaleWidth_xscale_finish, target_mc.scaleWidth_tween_speed, true);
	} else if (target_mc.scaleWidth_tween_type == 'Back.easeInOut') {
		target_mc.scaleWidth_tween = new Tween(target_mc, "_xscale", Back.easeInOut, target_mc.scaleWidth_xscale_start, target_mc.scaleWidth_xscale_finish, target_mc.scaleWidth_tween_speed, true);
	} else if (target_mc.scaleWidth_tween_type == 'Regular.easeIn') {
		target_mc.scaleWidth_tween = new Tween(target_mc, "_xscale", Regular.easeIn, target_mc.scaleWidth_xscale_start, target_mc.scaleWidth_xscale_finish, target_mc.scaleWidth_tween_speed, true);
	} else if (target_mc.scaleWidth_tween_type == 'Regular.easeOut') {
		target_mc.scaleWidth_tween = new Tween(target_mc, "_xscale", Regular.easeOut, target_mc.scaleWidth_xscale_start, target_mc.scaleWidth_xscale_finish, target_mc.scaleWidth_tween_speed, true);
	} else if (target_mc.scaleWidth_tween_type == 'Regular.easeInOut') {
		target_mc.scaleWidth_tween = new Tween(target_mc, "_xscale", Regular.easeInOut, target_mc.scaleWidth_xscale_start, target_mc.scaleWidth_xscale_finish, target_mc.scaleWidth_tween_speed, true);
	} else {
		//   
		//
		target_mc.scaleWidth_tween.onMotionChanged = function() {
		};
	}
	//
	target_mc.scaleWidth_tween.onMotionStopped = function() {
		if (target_mc.scaleWidth_callback) {
			target_mc.scaleWidthComplete();
		}
	};
}
//
