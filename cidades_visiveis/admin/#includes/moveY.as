﻿
function moveY(target_mc:MovieClip, moveY_y_finish:Number, moveY_callback:Boolean, moveY_y_start:Number, moveY_tween_type:String, moveY_tween_speed:Number) {
	if (moveY_y_start == undefined) {
		moveY_y_start = target_mc._y;
	}
	//                  
	if (moveY_tween_type == undefined) {
		moveY_tween_type = "Strong.easeInOut";
	}
	//                         
	if (moveY_tween_speed == undefined) {
		moveY_tween_speed = 0.6;
	}
	//                            
	target_mc.moveY_y_start = moveY_y_start;
	target_mc.moveY_y_finish = moveY_y_finish;
	target_mc.moveY_callback = moveY_callback;
	target_mc.moveY_tween_speed = moveY_tween_speed;
	target_mc.moveY_tween_type = moveY_tween_type;
	//
	if (target_mc.moveY_tween_type == 'Elastic.easeIn') {
		target_mc.moveY_tween = new Tween(target_mc, "_y", Elastic.easeIn, target_mc.moveY_y_start, target_mc.moveY_y_finish, target_mc.moveY_tween_speed, true);
	} else if (target_mc.moveY_tween_type == 'Elastic.easeOut') {
		target_mc.moveY_tween = new Tween(target_mc, "_y", ELastic.easeOut, target_mc.moveY_y_start, target_mc.moveY_y_finish, target_mc.moveY_tween_speed, true);
	} else if (target_mc.moveY_tween_type == 'Elastic.easeInOut') {
		target_mc.moveY_tween = new Tween(target_mc, "_y", Elastic.easeInOut, target_mc.moveY_y_start, target_mc.moveY_y_finish, target_mc.moveY_tween_speed, true);
	} else if (target_mc.moveY_tween_type == 'Strong.easeIn') {
		target_mc.moveY_tween = new Tween(target_mc, "_y", Strong.easeIn, target_mc.moveY_y_start, target_mc.moveY_y_finish, target_mc.moveY_tween_speed, true);
	} else if (target_mc.moveY_tween_type == 'Strong.easeOut') {
		target_mc.moveY_tween = new Tween(target_mc, "_y", Strong.easeOut, target_mc.moveY_y_start, target_mc.moveY_y_finish, target_mc.moveY_tween_speed, true);
	} else if (target_mc.moveY_tween_type == 'Strong.easeInOut') {
		target_mc.moveY_tween = new Tween(target_mc, "_y", Strong.easeInOut, target_mc.moveY_y_start, target_mc.moveY_y_finish, target_mc.moveY_tween_speed, true);
	} else if (target_mc.moveY_tween_type == 'Back.easeIn') {
		target_mc.moveY_tween = new Tween(target_mc, "_y", Back.easeIn, target_mc.moveY_y_start, target_mc.moveY_y_finish, target_mc.moveY_tween_speed, true);
	} else if (target_mc.moveY_tween_type == 'Back.easeOut') {
		target_mc.moveY_tween = new Tween(target_mc, "_y", Back.easeOut, target_mc.moveY_y_start, target_mc.moveY_y_finish, target_mc.moveY_tween_speed, true);
	} else if (target_mc.moveY_tween_type == 'Back.easeInOut') {
		target_mc.moveY_tween = new Tween(target_mc, "_y", Back.easeInOut, target_mc.moveY_y_start, target_mc.moveY_y_finish, target_mc.moveY_tween_speed, true);
	} else if (target_mc.moveY_tween_type == 'Bounce.easeOut') {
		target_mc.moveY_tween = new Tween(target_mc, "_y", Bounce.easeOut, target_mc.moveY_y_start, target_mc.moveY_y_finish, target_mc.moveY_tween_speed, true);
	} else if (target_mc.moveY_tween_type == 'Bounce.easeIn') {
		target_mc.moveY_tween = new Tween(target_mc, "_y", Bounce.easeIn, target_mc.moveY_y_start, target_mc.moveY_y_finish, target_mc.moveY_tween_speed, true);
	} else if (target_mc.moveY_tween_type == 'Bounce.easeInOut') {
		target_mc.moveY_tween = new Tween(target_mc, "_y", Bounce.easeInOut, target_mc.moveY_y_start, target_mc.moveY_y_finish, target_mc.moveY_tween_speed, true);
	} else if (target_mc.moveY_tween_type == 'Regular.easeIn') {
		target_mc.moveY_tween = new Tween(target_mc, "_y", Regular.easeIn, target_mc.moveY_y_start, target_mc.moveY_y_finish, target_mc.moveY_tween_speed, true);
	} else if (target_mc.moveY_tween_type == 'Regular.easeOut') {
		target_mc.moveY_tween = new Tween(target_mc, "_y", Regular.easeOut, target_mc.moveY_y_start, target_mc.moveY_y_finish, target_mc.moveY_tween_speed, true);
	} else if (target_mc.moveY_tween_type == 'Regular.easeInOut') {
		target_mc.moveY_tween = new Tween(target_mc, "_y", Regular.easeInOut, target_mc.moveY_y_start, target_mc.moveY_y_finish, target_mc.moveY_tween_speed, true);
	} else {
	}
	//
	//
	target_mc.moveY_tween.onMotionStopped = function() {
		if (target_mc.moveY_callback) {
			target_mc.moveYComplete();
		}
	};
}
//
