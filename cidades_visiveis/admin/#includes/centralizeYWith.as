﻿
function centralizeYWith(target_mc:MovieClip, target2_mc, y_tween:Boolean) {
	//trace(target_mc);
	//trace(target2_mc);
	target_mc.y_tween = y_tween;
	if (target2_mc._name) {
		target2_center_y = Math.ceil(target2_mc._height/2);
	} else if (target2_mc == "stage_height") {
		target2_center_y = Math.ceil(Stage.height/2);
	} else {
		target2_center_y = target2_mc;
	}
	target_mc.novo_y = Math.ceil((target2_center_y)-(target_mc._height/2));
	if (target_mc.y_tween) {
		moveY(target_mc, target_mc.novo_y);
	} else {
		target_mc._y = target_mc.novo_y;
	}
}
