﻿
//import flash.filters.BlurFilter;
function moveX(target_mc:MovieClip, moveX_x_finish:Number, moveX_callback:Boolean, moveX_x_start:Number, moveX_tween_type:String, moveX_tween_speed:Number) {
	if (moveX_x_start == undefined) {
		moveX_x_start = target_mc._x;
	}
	// 
	//trace(target_mc);
	//trace(moveX_x_finish);
	//trace(moveX_x_start);
	//                              
	if (moveX_tween_type == undefined) {
		moveX_tween_type = "Strong.easeInOut";
	}
	//                                      
	if (moveX_tween_speed == undefined) {
		moveX_tween_speed = 0.6;
	}
	//                                         
	target_mc.moveX_x_start = moveX_x_start;
	target_mc.moveX_x_finish = moveX_x_finish;
	target_mc.moveX_callback = moveX_callback;
	target_mc.moveX_tween_speed = moveX_tween_speed;
	target_mc.moveX_tween_type = moveX_tween_type;
	//
	if (target_mc.moveX_tween_type == 'Elastic.easeIn') {
		target_mc.moveX_tween = new Tween(target_mc, "_x", Elastic.easeIn, target_mc.moveX_x_start, target_mc.moveX_x_finish, target_mc.moveX_tween_speed, true);
	} else if (target_mc.moveX_tween_type == 'Elastic.easeOut') {
		target_mc.moveX_tween = new Tween(target_mc, "_x", Elastic.easeOut, target_mc.moveX_x_start, target_mc.moveX_x_finish, target_mc.moveX_tween_speed, true);
	} else if (target_mc.moveX_tween_type == 'Elastic.easeInOut') {
		target_mc.moveX_tween = new Tween(target_mc, "_x", Elastic.easeInOut, target_mc.moveX_x_start, target_mc.moveX_x_finish, target_mc.moveX_tween_speed, true);
	} else if (target_mc.moveX_tween_type == 'Strong.easeIn') {
		target_mc.moveX_tween = new Tween(target_mc, "_x", Strong.easeIn, target_mc.moveX_x_start, target_mc.moveX_x_finish, target_mc.moveX_tween_speed, true);
	} else if (target_mc.moveX_tween_type == 'Strong.easeOut') {
		target_mc.moveX_tween = new Tween(target_mc, "_x", Strong.easeOut, target_mc.moveX_x_start, target_mc.moveX_x_finish, target_mc.moveX_tween_speed, true);
	} else if (target_mc.moveX_tween_type == 'Strong.easeInOut') {
		target_mc.moveX_tween = new Tween(target_mc, "_x", Strong.easeInOut, target_mc.moveX_x_start, target_mc.moveX_x_finish, target_mc.moveX_tween_speed, true);
	} else if (target_mc.moveX_tween_type == 'Back.easeIn') {
		target_mc.moveX_tween = new Tween(target_mc, "_x", Back.easeIn, target_mc.moveX_x_start, target_mc.moveX_x_finish, target_mc.moveX_tween_speed, true);
	} else if (target_mc.moveX_tween_type == 'Back.easeOut') {
		target_mc.moveX_tween = new Tween(target_mc, "_x", Back.easeOut, target_mc.moveX_x_start, target_mc.moveX_x_finish, target_mc.moveX_tween_speed, true);
	} else if (target_mc.moveX_tween_type == 'Back.easeInOut') {
		target_mc.moveX_tween = new Tween(target_mc, "_x", Back.easeInOut, target_mc.moveX_x_start, target_mc.moveX_x_finish, target_mc.moveX_tween_speed, true);
	} else if (target_mc.moveX_tween_type == 'Bounce.easeInOut') {
		target_mc.moveX_tween = new Tween(target_mc, "_x", Bounce.easeInOut, target_mc.moveX_x_start, target_mc.moveX_x_finish, target_mc.moveX_tween_speed, true);
	} else if (target_mc.moveX_tween_type == 'Bounce.easeIn') {
		target_mc.moveX_tween = new Tween(target_mc, "_x", Bounce.easeIn, target_mc.moveX_x_start, target_mc.moveX_x_finish, target_mc.moveX_tween_speed, true);
	} else if (target_mc.moveX_tween_type == 'Bounce.easeOut') {
		target_mc.moveX_tween = new Tween(target_mc, "_x", Bounce.easeOut, target_mc.moveX_x_start, target_mc.moveX_x_finish, target_mc.moveX_tween_speed, true);
	} else if (target_mc.moveX_tween_type == 'Regular.easeIn') {
		target_mc.moveX_tween = new Tween(target_mc, "_x", Regular.easeIn, target_mc.moveX_x_start, target_mc.moveX_x_finish, target_mc.moveX_tween_speed, true);
	} else if (target_mc.moveX_tween_type == 'Regular.easeOut') {
		target_mc.moveX_tween = new Tween(target_mc, "_x", Regular.easeOut, target_mc.moveX_x_start, target_mc.moveX_x_finish, target_mc.moveX_tween_speed, true);
	} else if (target_mc.moveX_tween_type == 'Regular.easeInOut') {
		target_mc.moveX_tween = new Tween(target_mc, "_x", Regular.easeInOut, target_mc.moveX_x_start, target_mc.moveX_x_finish, target_mc.moveX_tween_speed, true);
	} else {
	}
	//
	/*
	target_mc.moveX_tween.onMotionChanged = function() {
	var filtersArr:Array = new Array();
	blurX_value = Math.ceil((target_mc.moveX_x_finish-target_mc._x)/4);
	blurY_value = Math.ceil((target_mc.moveY_y_finish-target_mc._y)/4);
	if (blurX_value<0) {
	blurX_value = (blurX_value)*-1;
	}
	if (blurY_value<0) {
	blurY_value = (blurY_value)*-1;
	}
	//trace(blurX_value);   
	var blur:BlurFilter = new BlurFilter(blurX_value, blurY_value);
	filtersArr.push(blur);
	target_mc.filters = filtersArr;
	};
	*/
	//
	target_mc.moveX_tween.onMotionStopped = function() {
		if (target_mc.moveX_callback) {
			target_mc.moveXComplete();
		}
	};
}
//
