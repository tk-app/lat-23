﻿
function resizeWidth(target_mc:MovieClip, resizeWidth_width_finish:Number, resizeWidth_callback:Boolean, resizeWidth_width_start:Number, resizeWidth_tween_type:String, resizeWidth_tween_speed:Number) {
	//trace(target_mc);
	if (resizeWidth_width_start == undefined) {
		resizeWidth_width_start = target_mc._width;
	}
	//                 
	if (resizeWidth_tween_type == undefined) {
		resizeWidth_tween_type = "Strong.easeInOut";
	}
	//                        
	if (resizeWidth_tween_speed == undefined) {
		resizeWidth_tween_speed = 0.6;
	}
	//                           
	target_mc.resizeWidth_width_start = resizeWidth_width_start;
	target_mc.resizeWidth_width_finish = resizeWidth_width_finish;
	target_mc.resizeWidth_callback = resizeWidth_callback;
	target_mc.resizeWidth_tween_speed = resizeWidth_tween_speed;
	target_mc.resizeWidth_tween_type = resizeWidth_tween_type;
	//
	if (target_mc.resizeWidth_tween_type == 'Elastic.easeIn') {
		target_mc.resizeWidth_tween = new Tween(target_mc, "_width", Elastic.easeIn, target_mc.resizeWidth_width_start, target_mc.resizeWidth_width_finish, target_mc.resizeWidth_tween_speed, true);
	} else if (target_mc.resizeWidth_tween_type == 'Elastic.easeOut') {
		target_mc.resizeWidth_tween = new Tween(target_mc, "_width", ELastic.easeOut, target_mc.resizeWidth_width_start, target_mc.resizeWidth_width_finish, target_mc.resizeWidth_tween_speed, true);
	} else if (target_mc.resizeWidth_tween_type == 'Elastic.easeInOut') {
		target_mc.resizeWidth_tween = new Tween(target_mc, "_width", Elastic.easeInOut, target_mc.resizeWidth_width_start, target_mc.resizeWidth_width_finish, target_mc.resizeWidth_tween_speed, true);
	} else if (target_mc.resizeWidth_tween_type == 'Strong.easeIn') {
		target_mc.resizeWidth_tween = new Tween(target_mc, "_width", Strong.easeIn, target_mc.resizeWidth_width_start, target_mc.resizeWidth_width_finish, target_mc.resizeWidth_tween_speed, true);
	} else if (target_mc.resizeWidth_tween_type == 'Strong.easeOut') {
		target_mc.resizeWidth_tween = new Tween(target_mc, "_width", Strong.easeOut, target_mc.resizeWidth_width_start, target_mc.resizeWidth_width_finish, target_mc.resizeWidth_tween_speed, true);
	} else if (target_mc.resizeWidth_tween_type == 'Strong.easeInOut') {
		target_mc.resizeWidth_tween = new Tween(target_mc, "_width", Strong.easeInOut, target_mc.resizeWidth_width_start, target_mc.resizeWidth_width_finish, target_mc.resizeWidth_tween_speed, true);
	} else if (target_mc.resizeWidth_tween_type == 'Back.easeIn') {
		target_mc.resizeWidth_tween = new Tween(target_mc, "_width", Back.easeIn, target_mc.resizeWidth_width_start, target_mc.resizeWidth_width_finish, target_mc.resizeWidth_tween_speed, true);
	} else if (target_mc.resizeWidth_tween_type == 'Back.easeOut') {
		target_mc.resizeWidth_tween = new Tween(target_mc, "_width", Back.easeOut, target_mc.resizeWidth_width_start, target_mc.resizeWidth_width_finish, target_mc.resizeWidth_tween_speed, true);
	} else if (target_mc.resizeWidth_tween_type == 'Back.easeInOut') {
		target_mc.resizeWidth_tween = new Tween(target_mc, "_width", Back.easeInOut, target_mc.resizeWidth_width_start, target_mc.resizeWidth_width_finish, target_mc.resizeWidth_tween_speed, true);
	} else if (target_mc.resizeWidth_tween_type == 'Regular.easeIn') {
		target_mc.resizeWidth_tween = new Tween(target_mc, "_width", Regular.easeIn, target_mc.resizeWidth_width_start, target_mc.resizeWidth_width_finish, target_mc.resizeWidth_tween_speed, true);
	} else if (target_mc.resizeWidth_tween_type == 'Regular.easeOut') {
		target_mc.resizeWidth_tween = new Tween(target_mc, "_width", Regular.easeOut, target_mc.resizeWidth_width_start, target_mc.resizeWidth_width_finish, target_mc.resizeWidth_tween_speed, true);
	} else if (target_mc.resizeWidth_tween_type == 'Regular.easeInOut') {
		target_mc.resizeWidth_tween = new Tween(target_mc, "_width", Regular.easeInOut, target_mc.resizeWidth_width_start, target_mc.resizeWidth_width_finish, target_mc.resizeWidth_tween_speed, true);
	} else {
		//   
		//
		target_mc.resizeWidth_tween.onMotionChanged = function() {
		};
	}
	//
	target_mc.resizeWidth_tween.onMotionStopped = function() {
		if (target_mc.resizeWidth_callback) {
			target_mc.resizeWidthComplete();
		}
	};
}
//
