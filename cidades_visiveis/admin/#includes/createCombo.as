﻿//requer combo_seta_down_mc na library

function createCombo(onde, nome, X, Y, W, H, classe, cor_base, initLabel, initValue, opLabel, opValue, nulo, nuloLabel) {

	onde[nome].removeMovieClip();
	onde.createEmptyMovieClip(nome, onde.getNextHighestDepth());
	onde[nome].valor = initValue;
	onde[nome]._x = X;
	onde[nome]._y = Y;
	
	myCombo_mc = onde[nome];
	
	drawBoxFill(myCombo_mc, 'bg_mc', 0, 0, W, 20, 100);
	var cor:Color = new Color(myCombo_mc.bg_mc);
	cor.setRGB(cor_base);
	
	createText(myCombo_mc,'caption', 10, 2, 12, 8, false, false, true);
	alvo_t =  myCombo_mc.caption;
	setaCSS(alvo_t);
	alvo_t.htmlText = "<p class='"+ classe +"'>"+ initLabel +"</p>";
	
	myCombo_mc.attachMovie('combo_seta_down_mc', 'combo_seta_down_mc', myCombo_mc.getNextHighestDepth());
	myCombo_mc.combo_seta_down_mc._x = W -15;
	myCombo_mc.combo_seta_down_mc._y = 7;	

	myCombo_mc.bg_mc.onde      = myCombo_mc;
	myCombo_mc.bg_mc.W         = W;
	myCombo_mc.bg_mc.H         = H;
	myCombo_mc.bg_mc.classe    = classe;
	myCombo_mc.bg_mc.cor_base  = cor_base;
	myCombo_mc.bg_mc.opLabel   = opLabel;
	myCombo_mc.bg_mc.opValue   = opValue;
	myCombo_mc.bg_mc.nulo      = nulo;
	myCombo_mc.bg_mc.nuloLabel = nuloLabel;
	myCombo_mc.bg_mc.onRelease = function(){
		existe = this.onde.opcoes_mc;
		if(existe == undefined){
			showComboOptions(this.onde, this.W, this.H, this.classe, this.cor_base, this.opLabel, this.opValue, this.nulo, this.nuloLabel);
		}else{
			this.onde.opcoes_mc.removeMovieClip();
		}
	}

}

function showComboOptions(onde_mc, W, H, classe, cor_base, opLabel, opValue, nulo, nuloLabel){
	
	nivel = onde_mc._parent.getNextHighestDepth();
	onde_mc.swapDepths(nivel);
	onde_mc.opcoes_mc.removeMovieClip();
	onde_mc.createEmptyMovieClip('opcoes_mc', onde_mc.getNextHighestDepth());
	onde_mc.opcoes_mc._x = 0;
	onde_mc.opcoes_mc._y = 20;
	
	drawBoxFill(onde_mc.opcoes_mc, 'bg_mc', 0, 0, W, H, 100);
	var cor:Color = new Color(onde_mc.opcoes_mc.bg_mc);
	cor.setRGB(cor_base);
	
	onde_mc.opcoes_mc.createEmptyMovieClip('lista_mc', onde_mc.opcoes_mc.getNextHighestDepth());
	quantos = opLabel.length;
	for(x=0; x<quantos; x++){
		id = opValue[x];
		nm = opLabel[x];
		cb = 'option_'+ x;
		createText(onde_mc.opcoes_mc.lista_mc,cb, 10, (x*12), 12, 8, false, false, true);
		alvo_t =  onde_mc.opcoes_mc.lista_mc[cb];
		setaCSS(alvo_t);
		alvo_t.htmlText = "<p class='"+ classe +"'>"+ nm +"</p>";
		bt = 'botao_'+ x;
		drawBoxFill(onde_mc.opcoes_mc.lista_mc, bt, 10, ((x*12)+4), (alvo_t._width +5), 10, 0);
		onde_mc.opcoes_mc.lista_mc[bt].id = id;
		onde_mc.opcoes_mc.lista_mc[bt].nm = nm;
		onde_mc.opcoes_mc.lista_mc[bt].cl = classe;
		onde_mc.opcoes_mc.lista_mc[bt].bg = onde_mc.bg_mc;
		onde_mc.opcoes_mc.lista_mc[bt].cb = cor_base;
		onde_mc.opcoes_mc.lista_mc[bt].onRelease = function(){
			onde_mc.valor = this.id;
			onde_mc.caption.htmlText = "<p class='"+ this.cl +"'>"+ this.nm +"</p>";
			var cor:Color = new Color(this.bg);
			cor.setRGB(this.cb);
			onde_mc.opcoes_mc.removeMovieClip();			
		}
	}	
	
	if(onde_mc.opcoes_mc.lista_mc._height > H){
		drawBoxFill(onde_mc.opcoes_mc.lista_mc, 'dummy', 10, ((quantos*12)+10), 100, 5, 0);
		drawBoxFill(onde_mc.opcoes_mc, 'mask_mc', 0, 0, (W -23), H, 10);
		onde_mc.opcoes_mc.lista_mc.setMask(onde_mc.opcoes_mc.mask_mc);
		scrollMcOnBtnHFixH(onde_mc.opcoes_mc.mask_mc, onde_mc.opcoes_mc.lista_mc, onde_mc.opcoes_mc)
	}
}