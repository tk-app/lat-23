﻿
function changeColor(target_mc:MovieClip, color_hex:Number, alpha:Number, callback:Boolean) {
	//trace(target_mc);
	//trace(color_hex);
	target_mc.callback = callback;
	new_color = new Color(target_mc);
	new_color.setRGB(color_hex);
	if (alpha) {
		target_mc._alpha = alpha;
	}
	if (target_mc.callback) {
		target_mc.changeColorComplete();
	}
}
