﻿
function loadXML(target_mc:MovieClip, content_xml:XML, file_xml:String) {
	//trace(target_mc);
	//trace(file_xml);
	//trace(content_xml);
	target_mc.content_xml = content_xml;
	target_mc.content_xml.ignoreWhite = true;
	target_mc.content_xml.onLoad = function() {
		target_mc.loadXMLComplete();
	};
	target_mc.content_xml.load(file_xml);
}
