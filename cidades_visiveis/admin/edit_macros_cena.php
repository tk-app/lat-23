<? 
 
	session_start();
	 
	if(isset($_SESSION["scaffold_id"])){ 
		$id = $_SESSION["scaffold_id"]; 
		if($id == 0 or $id == ""){ 
			header("Location: index.php"); 
		}else{ 
			include "connect.php";
		} 
	}else{ 
		$_SESSION["scaffold_id"] = ""; 
		header("Location: index.php"); 
	} 
 
?>

<html> 
<head> 
<title>bonacode scaffold</title> 
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"> 
<link rel="STYLESHEET" type="text/css" href="admin.css"> 
<script language="javascript" src="admin.js"></script> 

<script language="javascript"> 

	function valida_form(){ 

		var macro = document.formulario.macro.value;
		if(macro == 0){
			alert("é necessário escolher uma macro");
			return(false);
		}

		var cena = document.formulario.cena.value;
		if(cena == 0){
			alert("é necessário escolher uma cena");
			return(false);
		}

	} 

</script> 

</head> 
<body bgcolor="#ffffff"> 
 
<?
 
	$id  = $_GET["id"];
	$sql = "SELECT * FROM macros_cena WHERE id = $id";
	$res = mysql_query($sql, $conn);
	if($res){
		$quantos = mysql_num_rows($res);
		if($quantos == 1){
			$values = $res;
			$continua = true;
		}else{
			$continua = false;
		}
	}
 
	if($continua){
 
?>

<form method="POST" action="preview_update_macros_cena.php" name="formulario" id="formulario" enctype="multipart/form-data" onSubmit="return valida_form()"> 
<input type="hidden" name="id" value="<? echo $id; ?>" /> 
<table border="0" cellpadding="3" cellspacing="3"> 
<tr> 
   <td width="75"></td> 
   <td width="300"></td> 
   <td width="150"></td> 
</tr> 
<tr> 
   <td></td> 
   <td colspan="2" class="titulo">EDITAR MACRO_CENA</td> 
</tr> 
  <? 
  	$value = mysql_result($values, 0, 1);
	$value = stripslashes($value); 
	$value = str_replace('"', '&quot;', $value); 
  ?> 
  <? 
  	$value = mysql_result($values, 0, 2);
	$value = stripslashes($value); 
	$value = str_replace('"', '&quot;', $value); 
  ?> 

<input type="hidden" name="macro" value="<? echo $value; ?>" />

  <? 
  	$value = mysql_result($values, 0, 3);
	$value = stripslashes($value); 
	$value = str_replace('"', '&quot;', $value); 
  ?> 
<tr> 
  <td height="32" align="right" class="input_label">cena</td> 
  <td> 
     <select name="cena"> 
        <option value="0">escolha uma opção</option> 
		 <? 
			$sql = "SELECT id, titulo FROM cenas ORDER BY titulo";
			$res = mysql_query($sql, $conn);
			if($res){
			   $quantos = mysql_num_rows($res);
			   for($x=0; $x<$quantos; $x++){
			      $op_id = mysql_result($res, $x, 0);
			      $op_nm = mysql_result($res, $x, 1);
			      if($op_id == $value){
			      	$selected="selected=\"selected\"";
			      }else{
			      	$selected="";
			      }
				  echo "<option value=\"$op_id\" $selected>$op_nm</option> \n";
			   }
			}
		  ?> 
     </select> 
  </td> 
  <td></td> 
</tr> 
<tr><td></td><td></td><td></td></tr> 
<tr><td></td><td><input type="submit" value="CONTINUAR" /></td><td></td></tr> 
</table> 
</form> 
<?
	}else{
		echo "<span class=\"titulo\">REGISTRO INVÁLIDO</span><br/><br/>";
  		echo "<a href=\"list_macros_cena.php\">VOLTAR</a></br>";
	}
?>
 
</body> 
</html> 
