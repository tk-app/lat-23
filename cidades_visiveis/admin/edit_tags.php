<? 
 
	session_start();
	 
	if(isset($_SESSION["scaffold_id"])){ 
		$id = $_SESSION["scaffold_id"]; 
		if($id == 0 or $id == ""){ 
			header("Location: index.php"); 
		}else{ 
			include "connect.php";
		} 
	}else{ 
		$_SESSION["scaffold_id"] = ""; 
		header("Location: index.php"); 
	} 
 
?>

<html> 
<head> 
<title>Cidades Visíveis</title> 
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"> 
<link rel="STYLESHEET" type="text/css" href="admin.css"> 
<script language="javascript" src="admin.js"></script> 

<script language="javascript"> 

	function valida_form(){ 

		var tag = document.formulario.tag.value;
		if(tag.length < 1){
			alert("prencha o campo tag");
			return(false);
		}

	} 

</script> 

</head> 
<body bgcolor="#ffffff"> 
 
<?
 
	$id  = $_GET["id"];
	$sql = "SELECT * FROM tags WHERE id = $id";
	$res = mysql_query($sql, $conn);
	if($res){
		$quantos = mysql_num_rows($res);
		if($quantos == 1){
			$values = $res;
			$continua = true;
		}else{
			$continua = false;
		}
	}
 
	if($continua){
 
?>

<form method="POST" action="preview_update_tags.php" name="formulario" id="formulario" enctype="multipart/form-data" onSubmit="return valida_form()"> 
<input type="hidden" name="id" value="<? echo $id; ?>" /> 
<table border="0" cellpadding="3" cellspacing="3"> 
<tr> 
   <td width="75"></td> 
   <td width="300"></td> 
   <td width="150"></td> 
</tr> 
<tr> 
   <td></td> 
   <td colspan="2" class="titulo">EDITAR TAG</td> 
</tr> 
  <? 
  	$value = mysql_result($values, 0, 1);
	$value = stripslashes($value); 
	$value = str_replace('"', '&quot;', $value); 
  ?> 
<tr> 
  <td height="32" align="right" class="input_label">tag</td> 
  <td><input type="text" name="tag" size="40" value="<? echo $value; ?>" /></td> 
  <td></td> 
</tr> 
<tr><td></td><td></td><td></td></tr> 
<tr><td></td><td><input type="submit" value="CONTINUAR" /></td><td></td></tr> 
</table> 
</form> 
<?
	}else{
		echo "<span class=\"titulo\">REGISTRO INVÁLIDO</span><br/><br/>";
  		echo "<a href=\"list_tags.php\">VOLTAR</a></br>";
	}
?>
 
</body> 
</html> 
