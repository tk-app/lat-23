<? 
 
	session_start();
	 
	if(isset($_SESSION["scaffold_id"])){ 
		$id = $_SESSION["scaffold_id"]; 
		if($id == 0 or $id == ""){ 
			header("Location: index.php"); 
		}else{ 
			include "connect.php";
		} 
	}else{ 
		$_SESSION["scaffold_id"] = ""; 
		header("Location: index.php"); 
	} 
 
?> 
 
<html> 
<head> 
<title>Cidades Visíveis</title> 
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"> 
<link rel="STYLESHEET" type="text/css" href="admin.css"> 
<script language="javascript" src="admin.js"></script> 
</head> 
<body bgcolor="#ffffff"> 
 
<?
 
	$id  = $_POST["id"];
 
?>

<form method="POST" action="update_cameras.php"> 
<input type="hidden" name="id" value="<? echo $id; ?>" /> 
<table border="0" cellpadding="3" cellspacing="3"> 
<tr> 
   <td width="75"></td> 
   <td width="300"></td> 
   <td width="150"></td> 
</tr> 
<tr> 
   <td></td> 
   <td colspan="2" class="titulo">NOVA CAMERA</td> 
</tr> 
<? 
	$continue = true; 
  ?> 
<tr> 
  <td height="32" align="right" class="input_label">título</td><? 
     $titulo = $_POST["titulo"]; 
     $titulo = stripslashes($titulo); 
	 $clean_str = str_replace('"', '&quot;', $titulo); 
  ?> 
  <td class="input_value"><? echo $titulo; ?></td> 
  <input type="hidden" name="titulo" value="<? echo $clean_str; ?>" /> 
  <td></td> 
</tr> 
<tr> 
  <td height="32" align="right" class="input_label">url</td><? 
     $url = $_POST["url"]; 
     $url = stripslashes($url); 
	 $clean_str = str_replace('"', '&quot;', $url); 
  ?> 
  <td class="input_value"><? echo $url; ?></td> 
  <input type="hidden" name="url" value="<? echo $clean_str; ?>" /> 
  <td></td> 
</tr> 
<tr> 
  <td height="32" align="right" class="input_label">tipo</td><? 
     $tipo = $_POST["tipo"]; 
     $tipo = stripslashes($tipo); 
	 $clean_str = str_replace('"', '&quot;', $tipo); 
  ?> 
  <td class="input_value"><? echo $tipo; ?></td> 
  <input type="hidden" name="tipo" value="<? echo $clean_str; ?>" /> 
  <td></td> 
</tr>

<?
	if($tipo == 'jpg'){
		$img_src = 'jpg.php?url='. $url;
	}else if($tipo == 'mjpg'){
		$img_src = 'mjpg.php?url='. $url;
	}
?>

<tr>
  <td height="32" align="right" class="input_label">preview</td>
  <td colspan="2"><img src="<? echo $img_src; ?>" /></td>
</tr>
 
<tr> 
  <td height="32" align="right" class="input_label">latitude</td><? 
     $latitude = $_POST["latitude"]; 
     $latitude = stripslashes($latitude); 
	 $clean_str = str_replace('"', '&quot;', $latitude); 
  ?> 
  <td class="input_value"><? echo $latitude; ?></td> 
  <input type="hidden" name="latitude" value="<? echo $clean_str; ?>" /> 
  <td></td> 
</tr> 
<tr> 
  <td height="32" align="right" class="input_label">longitude</td><? 
     $longitude = $_POST["longitude"]; 
     $longitude = stripslashes($longitude); 
	 $clean_str = str_replace('"', '&quot;', $longitude); 
  ?> 
  <td class="input_value"><? echo $longitude; ?></td> 
  <input type="hidden" name="longitude" value="<? echo $clean_str; ?>" /> 
  <td></td> 
</tr> 
<tr> 
  <td height="32" align="right" class="input_label">hemisferio</td><? 
     $hemisferio = $_POST["hemisferio"]; 
     $hemisferio = stripslashes($hemisferio); 
	 $clean_str = str_replace('"', '&quot;', $hemisferio); 
  ?> 
  <td class="input_value"><? echo $hemisferio; ?></td> 
  <input type="hidden" name="hemisferio" value="<? echo $clean_str; ?>" /> 
  <td></td> 
</tr> 

<tr> 
  <td height="32"></td> 
  <td class="input_label"> tags </td> 
  <td></td> 
</tr>

<?
	$tags = $_POST['tags'];
	$qtt = count($tags);
	for($x=0; $x<$qtt; $x++){
		$tag_id = $tags[$x];
		$sql_2  = "SELECT tag FROM tags WHERE id = $tag_id";
		$res_2  = mysql_query($sql_2, $conn);
		$qts_2  = mysql_num_rows($res_2);
		if($qts_2 == 1){
			$tag_nm = mysql_result($res_2, 0, 0);		 
?>

<tr> 
  <td height="32" align="right" >
  	<input type="hidden" name="tags[]" value="<? echo $tag_id; ?>">
  </td> 
  <td class="input_value"> - <? echo $tag_nm; ?></td> 
  <td></td> 
</tr>

<?
		}
	}
?>

<tr> 
  <td height="32" align="right" class="input_label">publicado</td><? 
     $publicado = $_POST["publicado"]; 
	 if($publicado == 0){
	 	$label = "inativo";
	 }else if($publicado == 1){
	 	$label = "ativo";
	 }
  ?> 
  <td class="input_value"><? echo $label; ?></td> 
  <input type="hidden" name="publicado" value="<? echo $publicado; ?>" /> 
  <td></td> 
</tr> 
<?  
	if($continue == true){  
?>  
<tr><td></td><td></td><td></td></tr> 
<tr><td></td><td><input type="submit" value="ATUALIZAR CAMERA" /></td><td></td></tr> 
<?  
	}else{ 
?>  
<tr><td></td><td></td><td></td></tr> 
<tr><td></td><td><input type="button" value="VOLTAR" onClick="javascript:history.back()" /></td><td></td></tr> 
<?  
	} 
?>  
 
</body> 
</html> 
