<? 
 
	session_start();
	 
	if(isset($_SESSION["scaffold_id"])){ 
		$id = $_SESSION["scaffold_id"]; 
		if($id == 0 or $id == ""){ 
			header("Location: index.php"); 
		}else{ 
			include "connect.php";
		} 
	}else{ 
		$_SESSION["scaffold_id"] = ""; 
		header("Location: index.php"); 
	} 
 
?>

<html> 
<head> 
<title>bonacode scaffold</title> 
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"> 
<link rel="STYLESHEET" type="text/css" href="admin.css"> 
<script language="javascript" src="admin.js"></script> 

<script language="javascript"> 

	function valida_form(){ 
		
		var titulo = document.formulario.titulo.value;
		if(titulo.length < 4){
			alert("prencha o campo título");
			return(false);
		}

		var tag_1 = document.formulario.tag_1.value;
		if(tag_1 == 0){
			alert("é necessário escolher uma tag - 1");
			return(false);
		}

		var tag_2 = document.formulario.tag_2.value;
		if(tag_2 == 0){
			alert("é necessário escolher uma tag - 2");
			return(false);
		}

		var tag_3 = document.formulario.tag_3.value;
		if(tag_3 == 0){
			alert("é necessário escolher uma tag - 3");
			return(false);
		}

		var tag_4 = document.formulario.tag_4.value;
		if(tag_4 == 0){
			alert("é necessário escolher uma tag - 3");
			return(false);
		}

		var tag_5 = document.formulario.tag_5.value;
		if(tag_5 == 0){
			alert("é necessário escolher uma tag - 4");
			return(false);
		}

	} 

</script> 

</head> 
<body bgcolor="#ffffff"> 
 
<form method="POST" action="preview_sequencias.php" name="formulario" id="formulario" enctype="multipart/form-data" onSubmit="return valida_form()"> 
<input type="hidden" name="id" value="0" /> 
<table border="0" cellpadding="3" cellspacing="3"> 
<tr> 
   <td width="75"></td> 
   <td width="300"></td> 
   <td width="150"></td> 
</tr> 
<tr> 
   <td></td> 
   <td colspan="2" class="titulo">NOVA SEQUENCIA</td> 
</tr> 

<tr> 
  <td height="32" align="right" class="input_label">título</td> 
  <td><input type="text" name="titulo" size="40" /></td> 
  <td></td> 
</tr>

<tr> 
  <td height="32" align="right" class="input_label">cam / tag 1</td> 
  <td> 
     <select name="tag_1"> 
        <option value="0">escolha uma opção</option> 
		 <? 
			$sql = "SELECT id, tag FROM tags ORDER BY tag";
			$res = mysql_query($sql, $conn);
			if($res){
			   $quantos = mysql_num_rows($res);
			   for($x=0; $x<$quantos; $x++){
			      $op_id = mysql_result($res, $x, 0);
			      $op_nm = mysql_result($res, $x, 1);
				  echo"<option value=\"$op_id\">$op_nm</option> \n";
			   }
			}
		  ?> 
     </select> 
  </td> 
  <td></td> 
</tr> 

<tr> 
  <td height="32" align="right" class="input_label">trilha 1</td> 
  <td><input type="file" name="trilha_1" size="30" /></td> 
  <td class="input_label">mp3 | 10240 kb</td> 
</tr>

<tr> 
  <td height="32" align="right" class="input_label">cam / tag 2</td> 
  <td> 
     <select name="tag_2"> 
        <option value="0">escolha uma opção</option> 
		 <? 
			$sql = "SELECT id, tag FROM tags ORDER BY tag";
			$res = mysql_query($sql, $conn);
			if($res){
			   $quantos = mysql_num_rows($res);
			   for($x=0; $x<$quantos; $x++){
			      $op_id = mysql_result($res, $x, 0);
			      $op_nm = mysql_result($res, $x, 1);
				  echo"<option value=\"$op_id\">$op_nm</option> \n";
			   }
			}
		  ?> 
     </select> 
  </td> 
  <td></td> 
</tr>

<tr> 
  <td height="32" align="right" class="input_label">trilha 2</td> 
  <td><input type="file" name="trilha_2" size="30" /></td> 
  <td class="input_label">mp3 | 10240 kb</td> 
</tr>
 
<tr> 
  <td height="32" align="right" class="input_label">cam / tag 3</td> 
  <td> 
     <select name="tag_3"> 
        <option value="0">escolha uma opção</option> 
		 <? 
			$sql = "SELECT id, tag FROM tags ORDER BY tag";
			$res = mysql_query($sql, $conn);
			if($res){
			   $quantos = mysql_num_rows($res);
			   for($x=0; $x<$quantos; $x++){
			      $op_id = mysql_result($res, $x, 0);
			      $op_nm = mysql_result($res, $x, 1);
				  echo"<option value=\"$op_id\">$op_nm</option> \n";
			   }
			}
		  ?> 
     </select> 
  </td> 
  <td></td> 
</tr> 

<tr> 
  <td height="32" align="right" class="input_label">trilha 3</td> 
  <td><input type="file" name="trilha_3" size="30" /></td> 
  <td class="input_label">mp3 | 10240 kb</td> 
</tr>

<tr> 
  <td height="32" align="right" class="input_label">cam / tag 4</td> 
  <td> 
     <select name="tag_4"> 
        <option value="0">escolha uma opção</option> 
		 <? 
			$sql = "SELECT id, tag FROM tags ORDER BY tag";
			$res = mysql_query($sql, $conn);
			if($res){
			   $quantos = mysql_num_rows($res);
			   for($x=0; $x<$quantos; $x++){
			      $op_id = mysql_result($res, $x, 0);
			      $op_nm = mysql_result($res, $x, 1);
				  echo"<option value=\"$op_id\">$op_nm</option> \n";
			   }
			}
		  ?> 
     </select> 
  </td> 
  <td></td> 
</tr> 

<tr> 
  <td height="32" align="right" class="input_label">trilha 4</td> 
  <td><input type="file" name="trilha_4" size="30" /></td> 
  <td class="input_label">mp3 | 10240 kb</td> 
</tr>

<tr> 
  <td height="32" align="right" class="input_label">cam / tag 5</td> 
  <td> 
     <select name="tag_5"> 
        <option value="0">escolha uma opção</option> 
		 <? 
			$sql = "SELECT id, tag FROM tags ORDER BY tag";
			$res = mysql_query($sql, $conn);
			if($res){
			   $quantos = mysql_num_rows($res);
			   for($x=0; $x<$quantos; $x++){
			      $op_id = mysql_result($res, $x, 0);
			      $op_nm = mysql_result($res, $x, 1);
				  echo"<option value=\"$op_id\">$op_nm</option> \n";
			   }
			}
		  ?> 
     </select> 
  </td> 
  <td></td> 
</tr> 

<tr> 
  <td height="32" align="right" class="input_label">trilha 5</td> 
  <td><input type="file" name="trilha_5" size="30" /></td> 
  <td class="input_label">mp3 | 10240 kb</td> 
</tr>

<tr><td></td><td></td><td></td></tr> 
<tr><td></td><td><input type="submit" value="CONTINUAR" /></td><td></td></tr> 
</table> 
</form> 
 
</body> 
</html> 
