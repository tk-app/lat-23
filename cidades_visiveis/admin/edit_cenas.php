<? 
 
	session_start();
	 
	if(isset($_SESSION["scaffold_id"])){ 
		$id = $_SESSION["scaffold_id"]; 
		if($id == 0 or $id == ""){ 
			header("Location: index.php"); 
		}else{ 
			include "connect.php";
		} 
	}else{ 
		$_SESSION["scaffold_id"] = ""; 
		header("Location: index.php"); 
	} 
 
?>

<html> 
<head> 
<title>bonacode scaffold</title> 
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"> 
<link rel="STYLESHEET" type="text/css" href="admin.css"> 
<script language="javascript" src="admin.js"></script> 
<script language="javascript" src="jquery-1.4.2.min.js"></script> 

<script language="javascript"> 
	
	function updateTipo(){
		var tipo = document.formulario.tipo.value;
		if(tipo == 1){			
			$('#tr-1').show();
			$('#tr-2').show();
			$('#tr-3').show();
			$('#tr-4').show();
			$('#tr-5').show();
			$('#tr-6').show();
			$('#opt-1').hide();
			$('#opt-2').show();
			$('#opt-3').show();
			$('#opt-4').show();
			$('#opt-5').show();
			$('#opt-6').show();
			$('#opt-7').show();
		}
		if(tipo == 2){
			$('#tr-1').show();
			$('#tr-2').hide();
			$('#tr-3').hide();
			$('#tr-4').show();
			$('#tr-5').show();
			$('#tr-6').hide();
			$('#opt-1').hide();
			$('#opt-2').hide();
			$('#opt-3').hide();
			$('#opt-4').show();
			$('#opt-5').show();
			$('#opt-6').hide();
		}
		if(tipo == 3){
			$('#tr-1').show();
			$('#tr-2').show();
			$('#tr-3').hide();
			$('#tr-4').show();
			$('#tr-5').hide();
			$('#tr-6').hide();
			$('#opt-1').show();
			$('#opt-2').show();
			$('#opt-3').hide();
			$('#opt-4').hide();
			$('#opt-5').hide();
			$('#opt-6').hide();
		}
	}
	
	function valida_form(){ 

		var titulo = document.formulario.titulo.value;
		if(titulo.length < 4){
			alert("prencha o campo título");
			return(false);
		}

		var duracao = document.formulario.duracao.value;
		if(duracao < 20 || duracao > 60){
			alert("a duração deve ter entre 20 e 60 segundos");
			return(false);
		}
		
		var tipo = document.formulario.tipo.value;		
		
		if(tipo == 1 || tipo == 2){
			var tag_1 = document.formulario.tag_1.value;
			if(tag_1 == 0){
				alert("é necessário escolher uma tag - 1");
				return(false);
			}
		}
			
		
		if(tipo == 3){
			var tag_4 = document.formulario.tag_4.value;
			if(tag_4 == 0){
				alert("é necessário escolher uma tag - 4");
				return(false);
			}
		}		
		

		if(trilha != "" && trilha.lastIndexOf(".mp3") == -1){
			alert("a trilha precisa ser um arquivo mp3");
			return(false);
		}

	} 

</script> 

</head> 
<body bgcolor="#ffffff"> 
 
<?
 
	$id  = $_GET["id"];
	$sql = "SELECT * FROM cenas WHERE id = $id";
	$res = mysql_query($sql, $conn);
	if($res){
		$quantos = mysql_num_rows($res);
		if($quantos == 1){
			$values = $res;
			$continua = true;
		}else{
			$continua = false;
		}
	}
 
	if($continua){
 
?>

<form method="POST" action="preview_update_cenas.php" name="formulario" id="formulario" enctype="multipart/form-data" onSubmit="return valida_form()"> 
<input type="hidden" name="id" value="<? echo $id; ?>" /> 
<table border="0" cellpadding="3" cellspacing="3"> 
<tr> 
   <td width="75"></td> 
   <td width="300"></td> 
   <td width="150"></td> 
</tr> 
<tr> 
   <td></td> 
   <td colspan="2" class="titulo">EDITAR CENA</td> 
</tr> 
  <? 
  	$value = mysql_result($values, 0, 1);
	$value = stripslashes($value); 
	$value = str_replace('"', '&quot;', $value); 
  ?> 
<tr> 
  <td height="32" align="right" class="input_label">título</td> 
  <td><input type="text" name="titulo" size="40" value="<? echo $value; ?>" /></td> 
  <td></td> 
</tr> 
  <? 
  	$value = mysql_result($values, 0, 2);
	$value = stripslashes($value); 
	$value = str_replace('"', '&quot;', $value); 
  ?> 
<tr> 
  <td height="32" align="right" class="input_label">duracao</td> 
  <td><input type="text" name="duracao" size="40" value="<? echo $value; ?>" /></td> 
  <td></td> 
</tr> 
  <? 
  	$value = mysql_result($values, 0, 3);
	$value = stripslashes($value); 
	$value = str_replace('"', '&quot;', $value); 
	$ck1   = '';
	$ck2   = '';
	$ck3   = '';
	if($value == 1){
		$ck1 = "selected='selected'";
	}
	if($value == 2){
		$ck2 = "selected='selected'";
	}
	if($value == 3){
		$ck3 = "selected='selected'";
	}
  ?> 
<tr> 
  <td height="32" align="right" class="input_label">tipo</td> 
  <td>
	<select id="tipo" name="tipo" onchange="updateTipo()">
		<option value="1" <? echo $ck1; ?>>6 cameras</option>
		<option value="2" <? echo $ck2; ?>>1 grande / 1 ou 2 pequenas</option>
		<option value="3" <? echo $ck3; ?>>1 ou 2 pequenas / 1 grande</option>
	</select>
  </td>
  <td></td> 
</tr> 
  <? 
  	$value = mysql_result($values, 0, 4);
	$value = stripslashes($value); 
	$value = str_replace('"', '&quot;', $value); 
  ?> 
<tr id="tr-1"> 
  <td height="32" align="right" class="input_label">tag 1</td> 
  <td> 
     <select id="tag_1" name="tag_1"> 
        <option value="0">escolha uma opção</option> 
		 <? 
			$sql = "SELECT id, tag FROM tags ORDER BY tag";
			$res = mysql_query($sql, $conn);
			if($res){
			   $quantos = mysql_num_rows($res);
			   for($x=0; $x<$quantos; $x++){
			      $op_id = mysql_result($res, $x, 0);
			      $op_nm = mysql_result($res, $x, 1);
			      if($op_id == $value){
			      	$selected="selected=\"selected\"";
			      }else{
			      	$selected="";
			      }
				  echo "<option value=\"$op_id\" $selected>$op_nm</option> \n";
			   }
			}
		  ?> 
     </select> 
  </td> 
  <td><span id='opt-1'>opcional</span></td> 
</tr> 
  <? 
  	$value = mysql_result($values, 0, 5);
	$value = stripslashes($value); 
	$value = str_replace('"', '&quot;', $value); 
  ?> 
<tr id="tr-2"> 
  <td height="32" align="right" class="input_label">tag 2</td> 
  <td> 
     <select id="tag_2" name="tag_2"> 
        <option value="0">escolha uma opção</option> 
		 <? 
			$sql = "SELECT id, tag FROM tags ORDER BY tag";
			$res = mysql_query($sql, $conn);
			if($res){
			   $quantos = mysql_num_rows($res);
			   for($x=0; $x<$quantos; $x++){
			      $op_id = mysql_result($res, $x, 0);
			      $op_nm = mysql_result($res, $x, 1);
			      if($op_id == $value){
			      	$selected="selected=\"selected\"";
			      }else{
			      	$selected="";
			      }
				  echo "<option value=\"$op_id\" $selected>$op_nm</option> \n";
			   }
			}
		  ?> 
     </select> 
  </td> 
  <td><span id='opt-2'>opcional</span></td> 
</tr> 
  <? 
  	$value = mysql_result($values, 0, 6);
	$value = stripslashes($value); 
	$value = str_replace('"', '&quot;', $value); 
  ?> 
<tr id="tr-3"> 
  <td height="32" align="right" class="input_label">tag 3</td> 
  <td> 
     <select id="tag_3" name="tag_3"> 
        <option value="0">escolha uma opção</option> 
		 <? 
			$sql = "SELECT id, tag FROM tags ORDER BY tag";
			$res = mysql_query($sql, $conn);
			if($res){
			   $quantos = mysql_num_rows($res);
			   for($x=0; $x<$quantos; $x++){
			      $op_id = mysql_result($res, $x, 0);
			      $op_nm = mysql_result($res, $x, 1);
			      if($op_id == $value){
			      	$selected="selected=\"selected\"";
			      }else{
			      	$selected="";
			      }
				  echo "<option value=\"$op_id\" $selected>$op_nm</option> \n";
			   }
			}
		  ?> 
     </select> 
  </td> 
  <td><span id='opt-3'>opcional</span></td> 
</tr> 
  <? 
  	$value = mysql_result($values, 0, 7);
	$value = stripslashes($value); 
	$value = str_replace('"', '&quot;', $value); 
  ?> 
<tr id="tr-4"> 
  <td height="32" align="right" class="input_label">tag 4</td> 
  <td> 
     <select id="tag_4" name="tag_4"> 
        <option value="0">escolha uma opção</option> 
		 <? 
			$sql = "SELECT id, tag FROM tags ORDER BY tag";
			$res = mysql_query($sql, $conn);
			if($res){
			   $quantos = mysql_num_rows($res);
			   for($x=0; $x<$quantos; $x++){
			      $op_id = mysql_result($res, $x, 0);
			      $op_nm = mysql_result($res, $x, 1);
			      if($op_id == $value){
			      	$selected="selected=\"selected\"";
			      }else{
			      	$selected="";
			      }
				  echo "<option value=\"$op_id\" $selected>$op_nm</option> \n";
			   }
			}
		  ?> 
     </select> 
  </td> 
  <td><span id='opt-4'>opcional</span></td> 
</tr> 
  <? 
  	$value = mysql_result($values, 0, 8);
	$value = stripslashes($value); 
	$value = str_replace('"', '&quot;', $value); 
  ?> 
<tr id="tr-5"> 
  <td height="32" align="right" class="input_label">tag 5</td> 
  <td> 
     <select id="tag_5" name="tag_5"> 
        <option value="0">escolha uma opção</option> 
		 <? 
			$sql = "SELECT id, tag FROM tags ORDER BY tag";
			$res = mysql_query($sql, $conn);
			if($res){
			   $quantos = mysql_num_rows($res);
			   for($x=0; $x<$quantos; $x++){
			      $op_id = mysql_result($res, $x, 0);
			      $op_nm = mysql_result($res, $x, 1);
			      if($op_id == $value){
			      	$selected="selected=\"selected\"";
			      }else{
			      	$selected="";
			      }
				  echo "<option value=\"$op_id\" $selected>$op_nm</option> \n";
			   }
			}
		  ?> 
     </select> 
  </td> 
  <td><span id='opt-5'>opcional</span></td> 
</tr> 
  <? 
  	$value = mysql_result($values, 0, 9);
	$value = stripslashes($value); 
	$value = str_replace('"', '&quot;', $value); 
  ?> 
<tr id="tr-6"> 
  <td height="32" align="right" class="input_label">tag 6</td> 
  <td> 
     <select id="tag_6" name="tag_6"> 
        <option value="0">escolha uma opção</option> 
		 <? 
			$sql = "SELECT id, tag FROM tags ORDER BY tag";
			$res = mysql_query($sql, $conn);
			if($res){
			   $quantos = mysql_num_rows($res);
			   for($x=0; $x<$quantos; $x++){
			      $op_id = mysql_result($res, $x, 0);
			      $op_nm = mysql_result($res, $x, 1);
			      if($op_id == $value){
			      	$selected="selected=\"selected\"";
			      }else{
			      	$selected="";
			      }
				  echo "<option value=\"$op_id\" $selected>$op_nm</option> \n";
			   }
			}
		  ?> 
     </select> 
  </td> 
  <td><span id='opt-6'>opcional</span></td> 
</tr> 
  <? 
  	$value = mysql_result($values, 0, 10);
	$value = stripslashes($value); 
	$value = str_replace('"', '&quot;', $value); 
  ?> 
<tr> 
  <td height="32" align="right" class="input_label">trilha</td> 
  <td class="input_value"><a href="../conteudo/mp3/<? echo $value; ?>" target="_blank"><? echo $value; ?></a></td> 
  <td></td> 
  <input type="hidden" name="trilha" value="<? echo $value; ?>" /> 
</tr> 
<tr> 
  <td height="32" align="right" class="input_label">alterar trilha ?</td> 
  <td><input type="file" name="new_trilha" size="40" /></td> 
  <td class="input_label">mp3 | 10240 kb</td> 
</tr> 
<tr><td></td><td></td><td></td></tr> 
<tr><td></td><td><input type="submit" value="CONTINUAR" /></td><td></td></tr> 
</table> 
</form> 
<?
	}else{
		echo "<span class=\"titulo\">REGISTRO INVÁLIDO</span><br/><br/>";
  		echo "<a href=\"list_cenas.php\">VOLTAR</a></br>";
	}
?>

<script language="javascript"> 
	updateTipo();
</script>
 
</body> 
</html> 
